---
layout: axepromotion
title: Axe Promotion
subtitle: Formons-nous !
author: Axe Promotion
author_staff_member: z_axepromotion
image_path: "images/team/axepromotion.png"
---

## Notre projet

Depuis 2007, [AXE PROMOTION](https://www.axepromotion.com/){:target="_blank"} accompagne les personnes et les organisations dans leur développement de compétences en s’appuyant sur la formation, l’évaluation et le coaching. 

<br/>
<br/>

## Pourquoi <a href="{{ site.baseurl }}/"><img src="{{ site.baseurl }}/images/full_logo_blue.png" alt="CodeDuSoir" width="200" style="vertical-align:middle;"></a>?

[AXE PROMOTION](https://www.axepromotion.com/){:target="_blank"} a constitué une [équipe]({{ site.baseurl }}/team) exclusivement consacrée à l’informatique. 

Ce projet est né d’un constat simple : le numérique s’est installé dans nos vies et nous voulons donner les clés à nos enfants afin qu’ils en maîtrisent les codes.
