# Jekyll theme 

Urban theme from [CloudCannon Academy](https://learn.cloudcannon.com/).
- github : https://github.com/CloudCannon/urban-jekyll-template 
- Live Demo : https://teal-worm.cloudvent.net/

## Usage

[Jekyll](https://jekyllrb.com/) version 3.3.1

Install the dependencies with [Bundler](https://bundler.io/):

~~~bash
$ bundle install
~~~

Run `jekyll` commands through Bundler to ensure you're using the right versions:

~~~bash
$ bundle exec jekyll serve
~~~

# More 
 
## Online svg converter
- https://image.online-convert.com/fr/convertir-en-svg
- https://www.pngtosvg.com

## Online pixel converter
- http://convert-my-image.com/ImageConverter_Fr

