---
title: "Scratch - Mouvement couleur et son"
categories:
  - cours
  - scratch
  - fondamentaux
  - école et collège
image: "/images/formations/scratch-cours2.jpg"
author_staff_member: axepromotion
permalink: /fondamentaux-scratch/mouvement-couleur-son
---

<!-- more -->

Ce cours s’inscrit dans le cadre de [notre formation gratuite à distance à scratch](https://codedusoir.org/formations/twitch-scratch3/).
Nous voulons te faire découvrir la programmation en s’amusant et en renforçant tes connaissances en mathématiques, en français, en anglais... 

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"}.

## Préparation 

- D'abord, tu te rends dans ton compte scratch [à cette adresse](https://scratch.mit.edu/){:target="_blank"}.
- Tu vas également sur notre forum pour poser tes questions ou avoir accès à des liens, ou à d'autres ressources [à cette adresse](https://codedusoir.zulipchat.com){:target="_blank"}. C'est là que tu pourras discuter avec nous et demander de l'aide si tu as besoin.

## Mouvement

Tu vas écrire un bout de programme ou script pour faire avancer ou reculer ton lutin.
- Tu déposes le lutin et l'arrière-plan de ton choix sur la scène.
- Maintenant tu vas aller chercher les différents blocs de code dans les rubriques "mouvement" (blocs bleus)  et "contrôle" (blocs orange). 
- Tu peux également créer une boucle en allant à la rubrique "contrôle". 
- Sous la scène tu pourras orienter ton lutin en cliquant sur "direction".

Pour t'aider, observe bien l'image:

![](/images/formations/scratch_Mouvement.jpg)

## Couleurs

Tu vas écrire un bout de programme pour faire changer la couleur ou clignoter quand on appuie sur la barre d’espace.
- Pour mettre de la couleur et faire clignoter ton lutin, tu utiliseras dans l'onglet "code" les blocs "évènement", "apparence" et "contrôle".
- Pour créer une boucle tu iras à la rubrique "contrôle". 

Pour t'aider, observe bien l'image:

![](/images/formations/scratch_Mouvement_couleur.jpg)

## Son

Cette fois-ci, nous allons jouer avec le son.
- Tu vas tout d'abord déposer le lutin "canard" (duck en anglais), sur la scène.
- Ensuite tu vas accéder à la rubrique "évènement" pour démarrer. Pour mettre du son, il est évidemment nécessaire d'aller dans la rubrique "son" pour choisir le son et le volume. 
- Un des blocs  dans "contrôle" va te permettre de faire une boucle.
- Grâce à la liste de son, tu vas changer la voix du canard en choisissant, par exemple, celle de l'oie (goose en anglais).

Pour t'aider, observe bien l'image:

![](/images/formations/scratch_Mouvement_couleur_son.jpg)

## Pour les enseignants 

- Il est indispensable de créer un compte pour les éducateurs. Ainsi vous trouverez des ressources pour les enseignants.
- Il n'est pas nécessaire d'avoir un ordinateur pour chaque élève pour apprendre à coder. Codage se marie bien avec collaboration. Les développeurs vous le diront.
- Scratch n'est pas compatible avec l'Ipad. Si vous travail sur Apple, vous pouvez suivre ce cours et vous entraîner avec [Snap](https://snap.berkeley.edu/).

Petit rappel : il est préférable d'apprendre à coder dans sa langue maternelle, même si coder peut aider à apprendre une nouvelle langue.

## Et après...

Peux-tu choisir un lutin et lui faire dire quelque chose en t'enregistrant ?

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"} en direct.
Mais en attendant, je te demande de créer et d'intituler ton deuxième projet : . 
Tu m'enverras un message sur notre forum pour me dire quel personnage et quel arrière-plan tu as choisis.
N'oublie pas, si tu as besoin d'un coup de main, nous sommes là pour t'aider.

Merci d'avoir lu ce cours.

## Questionnaire 
Evalue tes connaissances et visualise ta progression [ici](https://form.codedusoir.org/node/15){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles

- [forum](https://codedusoir.zulipchat.com){:target="_blank"}
- [twitch](https://www.twitch.tv/codedusoir/){:target="_blank"}
- [edx](https://courses.edx.org/login?next=/courses/course-v1%3ADelftX%2BScratchTENGX%2B1T2019/courseware/e0ad11486de74ade95bbc327bfe84d7c/2717a2fdaeb94fa1bae1b4674ab55cde/%3Factivate_block_id%3Dblock-v1%253ADelftX%252BScratchTENGX%252B1T2019%252Btype%2540sequential%252Bblock%25402717a2fdaeb94fa1bae1b4674ab55cde){:target="_blank"}
