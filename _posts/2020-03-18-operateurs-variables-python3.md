---
title: Python - Opérateurs et Variables
categories:
  - cours
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Fondamentaux_Python-Operateurs_Variables.png"
image_alt: variable_cours_python3
video_url: "https://bittube.video/videos/embed/37c2d0fc-4742-4f78-a393-ce18ca141e32"
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/operateurs-variables
show_comments: true
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## PYTHON

Python est un des langages de programmation. Un langage de programmation est une notation conventionnelle destinée à formuler des algorithmes et produire des programmes informatiques qui les appliquent. D'une manière similaire à une langue naturelle, un langage de programmation est composé d'un alphabet, d'un vocabulaire, de règles et de significations. 
Python est l'un des langages les plus utilisés dans le monde. 
On l'utilise par exemple pour de l'automatisation de système, pour création d'I.A, pour la construction d'applications, des Websites, les data sciences et faire du machine learning. 

Python est un langage de programmation interprété. 
- Il faut savoir qu'un ordinateur ne peut lire que du code binaire.
    
On a donc besoin de transformer le code du langage de programmation en code binaire. 
- Pour ce faire les langages interprétés vont utilisez un interpréteur qui lira et transformera ligne par ligne le code éxécuté. 
- À la différence d'un langage de programmation compilé qui doit d'abord transformer tout le code en code binaire avant de pouvoir l'exécuter. C'est le compilateur qui transforme le code d'un langage de programmation compilé en code binaire.

## IDE

Un IDE ou EDI est un environnement de développement. On peut le comparer à un logiciel comme word. Mais au lieu de nous faciliter l'écriture d'un document texte(comme word le fait); l'IDE vous aide à écrire du code, en vous offrant des fonctions particulièrement utiles lorsque vous ecrivez du code.

Pour cette série sur les fondamentaux de Python nous utiliserons l'IDE "repl.it". C'est directement disponible en ligne prêt à l'usage! Il n'y a aucune installation à réaliser et tout fonctionne très facilement!

Vous pouvez aller à cette adresse [https://repl.it/](https://repl.it){:target="_blank"}. et cliquer sur New Repl en haut à droite.
Selectionnez ensuit le langage Python. puis cliquer sur "repl.it create".

Vous arrivez sur une nouvelle page. On distinge 4 parties:
- La barre de navigation: qui contient de gauche à droite le nom de votre repl.it, un bouton "invite", "run", "share", la possibilité de créer un nouveau repl.it, enfin un lien vers votre profil utilisateur.  
- La SIDE BAR avec en selection l'arborescence des documents.
- Au centre Le code du fichier selectionné. Tous les fichiers Python doivent avoir une extension '.py'.
- Enfin la console. La console c'est un peu comme un laboratoire. c'est ici que vous expérimentez et que vous pouvez intéragir directement avec Python. Vous pouvez écrire des lignes simples de code et Python vous répondra.

Voici la documentation de repl.it:
[https://docs.repl.it/misc/quick-start](https://docs.repl.it/misc/quick-start){:target="_blank"}


## LA CONSOLE ET LES OPERATEURS.

La console est un terminal dédié uniquement à l'envoi et au retour des commandes. Il s’agit d'un dispositif de communication homme-machine.

### Jouer avec la console : saisir des instructions

On saisit un nombre sur la console qui nous le renvoie

```python
>>> 7
7
```

```python
>>> 7.1
7.1
```

On peut faire la même chose avec une chaine de caractères.

```python
>>> 'bonjour'
'bonjour'
```

### Les opérations de base

##### Addition, soustraction

On peut addition ou soustraire simplement. On remarque que la partie décimal d'un nombre est exprimée avec un point.
```python
>>> 5 + 2
7
>>> -7 + 8
1
>>> 7.1 + 2
9.1
>>> 7.1 - 2
5.1
```

##### Multiplication, division

```python
>>> 5 / 2
2.5
```

```python
>>> 5 * 2
10
```

##### Division euclidienne

// permet d'obtenir la partie entière d'une division.
```python
>>> 5 // 2
2
```

L'opérateur « % », ou « modulo », permet de connaître le reste de la division.
```python
>>> 5 % 2
1
```

##### Les parenthèses

Les parenthèses peuvent aussi être utilisées dans le langage Python. Elles respectent la priorité des opérations utilisé en mathématiques:

```python
>>> 5 * 2 + 1
11
```

```python
>>> 5 * (2 + 1)
15
```

Par contre, vous ne pouvez pas faire d'opération entre des chaines de caractères et des nombres:

```python
>>> 5 * deux
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'deux' is not defined
```

## LES VARIABLES

Une variables est un container qui stocke une valeur. Cette valeur peut être un nombre ou une chaine de caractères. Chaque variable a un nom qui permet de l'identifier. On assigne une valeur à une variable comme ceci:

```python
>>> x = 5
```
```python
>>> phrase = "Bonjour, J'aime les cookie"
```

Attention vous ne pouvez pas assigner une valeur à une variable comme ceci 5 = x. Python vous renverra un message d'erreur.

```python
>>> 5 = x
  File "<stdin>", line 1
SyntaxError: cannot assign to literal
```
Comme vous le voyez, On indique à gauche du signe '=' le nom de la variable et à droite la valeur de la variable. Autrement Python nous renvoie une erreur.


Pour que Python vous renvoie la valeur de vos variables il vous suffit d'écrire le nom de la variable:

```python
>>> x
5
```

```python
>>> phrase
"Bonjour, J'aime les cookie"
```
Vous pouvez aussi utiliser la fonction print()

```python
>>> print(x)
 5
```
En programmation, on peut écrire ceci:

```python
>>> x = 5
>>> x = x + 1
>>> x
 6
```
Ici nous avons changé la valeur de x en incorporant x dans le calcul de la nouvelle valeur. Le x à droite du signe = représente l'ancienne valeur de la variable x. Ainsi Python interprète x = x + 1 comme x = 5 + 1. Et donc x vaut maintenant 6. 
Il faut bien faire attention à ne pas confondre l’égalité mathématique C = C + 1 qui est toujours fausse (ou l’équation d’inconnue C qui n’a pas de solution) avec la nouvelle affectation de la variable C à l’aide de l’ancienne valeur.

### Comparateurs

Python vous permet de faire des tests entre les variables. Ces tests se font avec des comparateurs:

| Comparateurs | Signification |
|==============|===============|
| x < y	| est ce que x est strictement inferieur a y |
| <= | est ce que x est inferieur ou egal a y |
| > | est ce que x est strictement superieur a y |
| >= | est ce que x est superieur ou egal a y |
| ==	| est ce que x est egal a y |
| !=	| est ce que x est different de y |

si le test est vrai, Python vous renvoie 'True' et 'False' si le test est faux.

```python
>>> x = 13
>>> x == 12
false
```
Pourquoi Python nous renvoie false? On a vu qu'une affectation se fait en utilisant le signe =. L'opérateur '==' teste si une égalité est vraie ou fausse. Il vaut renvoie ensuite "false" ou "true".

On a aussi les opérateurs de comparaison '<', '>', '<=', et  '>='. Ils renvoient 'true' si la valeur de gauche respectivement est inférieur, supérieur, inférieur ou égal, supérieur ou égal à la valeur de droite.

```python
>>> x = 5
>>> x < 12
True
```

```python
>>> x = 5
>>> x > 5
False
```

```python
>>> x = 5
>>> x <= 5
True
```

```python
>>> x = 13
>>> x <= 12
False
```

```python
>>> x = 13
>>> x >= 12
True
```


```python
>>> x = 13
>>> x != 12
True
```

Dans le dernier exemple, on demande a Python si x est diff2rent de 12. On a True car x vaut 13.
 
Le seul opérateur que l'on peut utiliser avec un nombre et une chaine de caractères est l'astérisque de la multiplication. 

```python
>>>  13 * 'dix'
'dixdixdixdixdixdixdixdixdixdixdixdixdix'
```


```python
>>>  13 - 'dix'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for -: 'int'and 'str'
```

```python
>>>  13 + 'dix'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for -: 'int'and 'str'
```

```python
>>>  13 / 'dix'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for -: 'int'and 'str'
```

{% include footer_python.html %}

Merci d'avoir lu ce premier cours. À très vite pour les types de variables et les manipulations de string.

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/4){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
