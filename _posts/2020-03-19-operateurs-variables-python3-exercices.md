---
title: Python - Opérateurs et Variables
categories:
  - exercices
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Fondamentaux_Python-Operateurs_Variables_Exercices.png"
image_alt: variable_exercices_python3
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/operateurs-variables-exercices
show_comments: true
---

<!-- more -->

{% include header_python.html %}

## Exercices

#### Calcule le résultat des opérations suivantes
- Le reste de la division euclidienne de sept plus un divisé par cinq
- Le produit de cinq  et dix divisé par six
- La différence de cinq moins deux le tout multiplié par l’addition de sept avec trois
- Le quotient de la division euclidienne de quatre-vingt deux divisé par sept

#### Manipule les variables dans la console
- Assignez aux variables x, y, z la valeur 1, 2 et 3
- Affichez les valeurs des variables x et y 
- Assignez à la variable z la valeur de la variable x. Assignez à la variable x la valeur de la variable y. Comparez la valeur de x avec y et de z avec y avec l’opérateur adéquat, Quel retours avez vous ? 
- Quels sont les valeurs de x, y et z ?

#### Les globules rouges

Il y a 2,025 x 10^14 globules rouges dans 5,5litres de sang du corps humain.
Combien y a t-il de globules rouges dans 200mL de sang contenu dans une poche de sang ?

#### Les citernes

D'une citerne contenant 1200 litres d'eau, on a soutiré 45 litres, puis 169 litres et enfin 237 litres.
Quelle quantité d'eau reste t-il dans la citerne ?

### Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/4){:target="_blank"}. 

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
