---
title: "Scratch - Des listes et des costumes"
categories:
  - cours
  - scratch
  - fondamentaux
  - école et collège
image: "/images/formations/scratch-cours4.jpg"
author_staff_member: axepromotion
permalink: /fondamentaux-scratch/liste-et-costume
---

<!-- more -->

Ce cours s’inscrit dans le cadre de [notre formation gratuite à distance à scratch](https://codedusoir.org/formations/twitch-scratch4/).
Nous voulons te faire découvrir la programmation en s’amusant et en renforçant tes connaissances en mathématiques, en français, en anglais... 

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"}.

## Les listes déconnectées

Déjà la quatrième semaine du parcours. Cette semaine, nous apprenons un nouveau concept de programmation. Ce que nous n'avons pas encore fait, c'est travailler avec des données. Les ordinateurs sont doués pour ça : le traitement des données. C'est ce que nous abordons cette semaine dans la première partie de la séance en travaillant sur les listes. 
Pensez à une liste qui vous est familière... Penser à des listes que vous rencontrez dans votre vie quotidienne. 

Quelles listes connaissez-vous ? Quelle liste pouvez-vous trouver ? 
Une liste qui vous est familière est bien sûr votre liste de classe, avec tous les noms de vos élèves. 
Quelles sont les autres listes contenant **des données** que vous rencontrez dans la vie quotidienne ? 
Remplissez 4 exemples de listes que vous connaissez. Plus tard, vous pourrez discuter sur le forum pour savoir si vous êtes d'accord.
Ce n'est pas très difficile, n'est-ce pas ? Les listes sont partout. 
Cet exercice est un exercice que vous pouvez très bien faire en classe: demander ce que doit figurer sur une liste. 
Vous pouvez alors trouver beaucoup de choses, vous le remarquerez. Les amis, les passe-temps, les aliments préférés, les choses que vous voyez en classe... 
C'est ainsi que vous pouvez vous habituer à l'idée que de travailler avec des listes. Et vous essaierez cela plus tard dans Scratch

## Pratique

- D'abord, rndons-nous sur notre compte scratch [à cette adresse](https://scratch.mit.edu/){:target="_blank"}.
- Allons également sur notre forum pour poser tes questions ou avoir accès à des liens, ou à d'autres ressources [à cette adresse](https://codedusoir.zulipchat.com){:target="_blank"}. C'est là que tu pourras discuter avec nous et demander de l'aide si tu as besoin.
- Avant de commencer nous nous rendons sur mon "compte enseignant" pour visualiser les ressources mises à disposition.

**Dans le 4ème cours, en pratique nous utilisons les blocs pour constituer des listes :** 
Pour accéder à ce qui va permettre de créer une liste, il faut se rendre aux blocs "variables". Sous les blocs "variables", on remarque un petit onglet "créer une liste". 
Avant de cliquer dessus observons bien les blocs "variables" pour voir ce qu'il va se passer... 
Maintenant, cliquons et nommons notre liste "animaux", par exemple.
Que se passe-t-il du côté des blocs et sur la scène ?
Soyons curieux ! Nous allons tester les blocs de toute forme, insérer des animaux dans la liste, faire parler scratch par le biais de bulles...
Amusez-vous !

**Puis nous utilisons les costumes à partir de l'onglet "costumes"** pour que le sprite que nous allons  appeler "Julie" puisse se déplacer sur la plage.
Elle a rendez-vous avec scratch et elle réalise qu'elle est en retard... 
Elle réfléchit et se dit qu'elle doit se dépêcher...

Nous avons utilisé les **blocs jaunes "évènement"**, **les blocs violets "apparence"**, **les blocs orange "contrôle"** pour la boucle, notamment, **les blocs bleus "mouvement"**.

Peux-tu créer un programme, à partir d'une histoire que tu auras créée, en t'aidant de l'image suivante (essaie de décomposer le programme pour comprendre son évolution) :

![](/images/formations/scratch-cours4-pratique.jpg)

Tu peux aussi utiliser ta voix pour faire parler ton lutin. Tu peux lui faire faire demi-tour quand Julie arrive au bout de la scène... Bref ! utilise tout ce que ru sais le faire ;-)

N'oublie pas que tu peux demander de l'aide sur le forum

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"} en direct.

Tu m'enverras un message sur notre forum pour partager tes impressions et tes réalisations. Je suis très intéressée 
N'oublie pas, si tu as besoin d'un coup de main, nous sommes là pour t'aider.

Merci d'avoir lu ce cours.

## Questionnaire 
Evalue tes connaissances et visualise ta progression [ici](https://form.codedusoir.org/node/17){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles

- [forum](https://codedusoir.zulipchat.com){:target="_blank"}
- [twitch](https://www.twitch.tv/codedusoir/){:target="_blank"}
- [edx](https://courses.edx.org/login?next=/courses/course-v1%3ADelftX%2BScratchTENGX%2B1T2019/courseware/e0ad11486de74ade95bbc327bfe84d7c/2717a2fdaeb94fa1bae1b4674ab55cde/%3Factivate_block_id%3Dblock-v1%253ADelftX%252BScratchTENGX%252B1T2019%252Btype%2540sequential%252Bblock%25402717a2fdaeb94fa1bae1b4674ab55cde){:target="_blank"}