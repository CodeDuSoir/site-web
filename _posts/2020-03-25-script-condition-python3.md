---
title: Python - Script et Condition
categories:
  - cours
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Script-condition-python3.png"
image_alt: python-script-condition
video_url: https://bittube.video/videos/embed/01a90dad-91c0-4607-83ab-4cab6b43e62b
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/script-conditions
show_comments: true
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## Les scripts

### Définition

Un script est une succession d'instructions simples qui lancées seront exécutées l'une à la suite de l'autre. À la manière d'un « script » de théâtre.

Les commandes que nous écrivons dans la console sont des scripts d'une ligne !

### Utilisation 

La console n'est plus notre unique terrain de jeu !
Nous utiliserons maintenant aussi la partie script de repl.it. Ecrivez ceci dans la partie script.

```python
1 premierScript = 'Hello world'
2 print(premierScript)
```
Pour exécuter le script lorsque l'on est sur repl.it, vous devez lancer la commande 'run' qui se trouve dans la barre de navigation en haut de l'ecran.

### Attention Console != Script

Vous pouvez aussi utiliser la console pour inscrire plusieurs instructions à la suite les unes des autres. On doit séparer alors les instructions par des ";". Il est recommandé d'utiliser la partie script de python pour écrire une suite d'instruction. Car dans la console cela devient trop vite illisible.

```python
>>> x = 5; print(x)
```

Dans la partie script nous ne pouvons pas demander la valeur d'une variable sans la fonction print(). 
Écrire, dans le script la commande ci-dessous n'affichera rien.

```python
x
```

Il faut écrire
```python
print(x)
```

### Règles et Conventions de syntaxe

#### Règles 

N'écrivez qu'une commande par ligne!

```python
premierScript = 'Hello world' print(premierScript)
```

Cela vous renvoie une erreur de Syntax. On écrit donc:

```python
premierScript = 'Hello world'
print(premierScript)
```
#### Conventions

Garder toujours en tête que:

- Un code dont on est l'auteur peut être difficile à relire si on l'abandonne quelque temps.
- Lire le code d'un autre développeur est toujours plus délicat.
- Si votre code doit être utilisé par d'autres, il doit être facile à reprendre (à lire et à comprendre).

Les conventions sont là pour diminuer ces problèmes. Elles ne sont pas nécessaires mais elles restent très pratiques et recommandées. Voici la première convention que l'on recommande d'utiliser :


- Écrire en camelCase les variables. 
Il n'est pas possible d'écrire une variable avec des espaces. Ainsi communément les noms de variables commencent par une minuscule et les mots suivants sont accollées avec une majuscule :

```python
nombreDeMarshmallow = 3
```

D'autres conventions de syntaxe existent. Nous les verrons plus tard. Sachez que les règles et conventions de syntaxe changent d'un langage de programmation à l'autre. 


## Les conditions if

### Définition

Les conditions permettent d'exécuter une ou plusieurs instructions si une ou plusieurs conditions sont valides.
Nous allons voir aussi l'indentation qui consiste en l'ajout de tabulations ou d'espaces dans un fichier, pour une meilleure lecture et compréhension du code.

### Notation
Pour utiliser une condition if: on écrit le if puis on met la condition que l'on veut tester entre parenthèse. Puis on indique à Python que ce qui suit sont les instructions à suivre si la condition est valide. On indente les instructions pour que python comprenne que les instructions sont dans le if.

```python
x = 3
if x < 0:
    print("La valeur de x est negative")
if x > 0:
    print("La valeur de x est positive")
```

### If else

Cette méthode n'est pas optimale, tout d'abord parce qu'elle nous oblige à écrire deux conditions séparées pour tester une même variable. De plus, et même si c'est dur à concevoir par cet exemple, dans le cas où la variable remplirait les deux conditions (ici c'est impossible bien entendu), les deux portions de code s'exécuteraient.
On va donc utiliser l'instruction else, "sinon" en francais
```python
x = 3
if x < 0:
    print("La valeur de x est negative")
else:
    print("La valeur de x est positive")
```

### L'instruction elif:

Le mot clée elif est une contraction de « else if », que l'on peut traduire très littéralement par « sinon si ». Dans l'exemple que nous venons juste de voir, l'idéal serait d'écrire :

```python
x = 3
if x < 0:
    print("La valeur de x est negative")
elif x > 0:
    print("La valeur de x est positive ou egal à zero")
else:
    print("La valeur de x est egal à zero")
```

{% include footer_python.html %}

Merci d'avoir lu ce premier cours. À très vite pour les types de variables et les manipulations de string.

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/4){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
