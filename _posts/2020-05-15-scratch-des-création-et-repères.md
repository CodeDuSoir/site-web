---
title: "Scratch - Création et repères"
categories:
  - cours
  - scratch
  - fondamentaux
  - école et collège
image: "/images/formations/scratch-cours5.jpg"
author_staff_member: axepromotion
permalink: /fondamentaux-scratch/creation-et-repères
---

<!-- more -->

Ce cours s’inscrit dans le cadre de [notre formation gratuite à distance à scratch](https://codedusoir.org/formations/twitch-scratch4/).
Nous voulons te faire découvrir la programmation en s’amusant et en renforçant tes connaissances en mathématiques, en français, en anglais... 

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"}.

## La création

Piet Mondrian (1872–1944) est l’un des fondateurs de l’abstraction historique, et la grande figure du mouvement De Stijl à la recherche d’un art total. 
Je vous parle de Mondrian... Pourquoi?
Parce qu'il est le maître de la ligne droite, du quadrillage parfait et des couleurs primaires (bleu primaire, noir, blanc, jaune et rouge). 

L’artiste néerlandais visait la création d’un langage pictural universel, néanmoins radical, sans négliger la dimension spirituelle. 
Aux côtés de Vassily Kandinsky et de Kasimir Malevitch, mais dans un style bien différent, Mondrian a consacré la naissance de l’art moderne.
Pour ce qui est des couleurs, il a été touché par un autre peintre : Vincent van Gogh.

Il a dit : *« Je construis des lignes et des combinaisons de couleurs sur des surfaces planes afin d’exprimer, avec la plus grande conscience, la beauté générale. »*
Je me suis dit : *« On peut faire ça avec scratch... »* :-)

![](/images/formations/scratch-cours5-Mondrian.jpg)

## Les repères sur la scène

Dans un deuxième temps, nous allons essayer de nous repérer sur la scène pour positionner avec précision les lutins.
Pour commencer, il est important de connaître les dimensions de la scène. Elle mesure 480 pixel de large et 360 de haut.

Vous m'avez demandé : *« Mais c'est quoi un pixel ? »*
Une image au format numérique est un ensemble de points de couleur, nommés **pixels**. 
Chaque pixel est localisable par ses coordonnées dans l'image (position sur la ligne et dans la colonne). 
Quand les pixels sont suffisamment petits, on ne les distingue pas séparément les uns des autres.

Nous plaçons les lutins sur un **repère orthonormé**. 
Un repère orthonormé permet la localisation d’un point situé sur un plan comme dans le jeu de la bataille navale.
Nous allons savoir exactement où se trouve le lutins sur la scène.

C'est en choisissant "xy grid" dans l'arrère-plan que nous allons voir à quoi ça ressemble
Là ou x=0 et y=0, coordonnées du centre, c'est le centre de la scène
x au maximum 240 est positif à droite et négatif à gauche
y au maximum 180  est positif en haut et négatif en bas
C'est le bloc bleu "aller à x-y dans "mouvement" qui permet de programmer les emplacements.

![](/images/formations/scratch-cours5-reperes orthonormes.jpg)

Quant au nombre aléatoire, il s'agit d'un nombre dont le choix est le fait du hasard.
Si on veut que les lutins se placent sur la scène, au hasard, il suffit d'aller dans le bloc bleu "aller à x-y dans "mouvement" qui permet de programmer les emplacements et de glisser le bloc vert "nombre aléatoire entre (et tu choisis les nombres) que tu trouves dans "opérateurs".


## Pratique

- D'abord, rendons-nous sur notre compte scratch [à cette adresse](https://scratch.mit.edu/){:target="_blank"}.
- Allons également sur notre forum pour poser tes questions ou avoir accès à des liens, ou à d'autres ressources [à cette adresse](https://codedusoir.zulipchat.com){:target="_blank"}. C'est là que tu pourras discuter avec nous et demander de l'aide si tu as besoin.
- Avant de commencer nous nous rendons sur mon "compte enseignant" pour visualiser les ressources mises à disposition.

**Dans le 5ème cours, en pratique nous utilisons l'arrière plan pour dessiner 
Nous survolons l'icône de l'arrière plan pour voir apparaître différentes icônes et nous cliquons sur le petit pinceau (peindre).
En utilisans le rectangle, le remplissage et en supprimant le contour, nous dessinons le ciel et la mer. Nous faisons la même chose pour dessiner le soleil.
Puis en utilisant le pinceau, nous dessinons les contours de la banquise que nous remplirons en cliquant sur le pot de peinture puis en choisissant la couleur.

Nous survolons l'icône des lutins pour voir apparaître différentes icônes et nous cliquons sur le petit pinceau (peindre).
Et là, nous pouvons comme Mondrian le faisait, dessiner des figures géométriques que nous remplissons de couleurs primaires. 
On peut aussi faire apparaître le pourtour des figures en jouant avec l'icône pourtour.

Amusez-vous !

Peux-tu établir un programme en créant un arrière plan sur lequel tu peux introduire un personne conçu avec des cercles et des rectangles et essaie de placer ton personnage au centre de la scène :

![](/images/formations/scratch-cours 5-pratique.jpg)

Tu peux aussi utiliser ta voix pour faire parler ton lutin. Tu peux lui faire faire demi-tour quand Julie arrive au bout de la scène... Bref ! utilise tout ce que tu sais le faire ;-)

N'oublie pas que tu peux demander de l'aide sur le forum

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"} en direct.

Tu m'enverras un message sur notre forum pour partager tes impressions et tes réalisations. Je suis très intéressée 
N'oublie pas, si tu as besoin d'un coup de main, nous sommes là pour t'aider.

Merci d'avoir lu ce cours.

## Questionnaire 
Evalue tes connaissances et visualise ta progression [ici](https://form.codedusoir.org/node/17){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles

- [forum](https://codedusoir.zulipchat.com){:target="_blank"}
- [twitch](https://www.twitch.tv/codedusoir/){:target="_blank"}
- [edx](https://courses.edx.org/login?next=/courses/course-v1%3ADelftX%2BScratchTENGX%2B1T2019/courseware/e0ad11486de74ade95bbc327bfe84d7c/2717a2fdaeb94fa1bae1b4674ab55cde/%3Factivate_block_id%3Dblock-v1%253ADelftX%252BScratchTENGX%252B1T2019%252Btype%2540sequential%252Bblock%25402717a2fdaeb94fa1bae1b4674ab55cde){:target="_blank"}