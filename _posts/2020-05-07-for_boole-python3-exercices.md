---
title: "Python - Boucle bornée For et algèbre de Boole Exercices"
categories:
  - exercices
  - python
image: "/images/formations/Fondamentaux_Python-for_boole-exercices.png"
image_alt: For-loop_Boole_exercices_python3
author_staff_member: quentin-parrot
show_comments: true
---

<!-- more -->
Ce cours s'inscrit dans le cadre de notre [formation gratuite à distance à Python](https://www.codedusoir.org/formations/twitch/).

Nous voulons te faire découvrir la programmation en s’amusant et en renforcant tes connaissances en mathématiques.
Retrouve nous les mardis et vendredis à 16h sur [twitch](https://stream.codedusoir.org){:target="_blank"} en direct.


## Exercices

#### Boucle bornée For

1. En utilisant la boucle for créez une fonction print_all(liste). Cette fonction affiche tous les éléments de la liste passée en paramètre de la fonction. 
2. En utilisant la boucle for créez une fonction sum_all(liste). Cette fonction additionne tous les éléments de la liste passée en paramètre. 

### Algèbre de Boole

1. Créez une fonction and_validation(arg1, arg2) qui prend en paramètre deux booleans. Cette fonction vous renvoie True si et seulement si les deux booleans sont vrais. sinon elle vous renvoie False.
2. Créez une fonction number_check(nb1, nb2) qui affiche la soustraction de nb1 par nb2 si et seulement si nb1 est plus grand que nb2 et nb2 est superieur a 50, sinon la fonction renvoie nb1 + nb2. 


### Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/18){:target="_blank"}. 

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}

