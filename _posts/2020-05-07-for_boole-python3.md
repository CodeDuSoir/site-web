---
title: "Python - Boucle bornée for et algèbre de Boole" 
categories:
  - cours
  - python
image: "/images/formations/Fondamentaux_Python-for_boole-python3.png"
image_alt: For_loop-Boole_python3
author_staff_member: quentin-parrot
show_comments: true
---

<!-- more -->
Ce cours s'inscrit dans le cadre de notre [formation gratuite à distance à Python](https://www.codedusoir.org/formations/twitch/).

Nous voulons te faire découvrir la programmation en s’amusant et en renforcant tes connaissances en mathématiques.
Retrouve nous les mardis et vendredis à 16h sur [twitch](https://stream.codedusoir.org){:target="_blank"} en direct.

## Les boucles bornées : For

La boucle for permet, comme la boucle while, de répéter une action plusieurs fois. La différence avec la boucle while est que la boucle for opère sur une intervalle donnée.

```python
for item in intervalle:
    print(item)
```

Cette 'intervalle' doit donc être un élément itérable.

les éléments itérables sont entre autre les listes, les string et les dictionnaires, etc..

```python
poids_des_legumes = [1, 3, 2, 4]

for poids in poids_des_legumes:
    print(poids)
```

Attention, poids est un nom arbitraire qui nous permet de stocker les valeurs de poids_des_legumes une a une dans poids.

## L’algèbre de Boole

L'algèbre de Boole est un ensemble de règles de calcul de logique combinatoire. Ces opérations logiques permettent d'améliorer les performances de nos programmes. 

### And

Nous avons déjà vu les instructions if et while. Pour entrer dans le bloc d'instructions, une condition doit être validée. Mais comment faire si l'on veut valider la condition si et seulement si deux conditions sont vérifiées? avec le mots clefs **and**!

```python
isTrue = True
i = 0
if(i < 5 and isTrue == True):
    print('Les conditions sont valides')
else:
    print("L'une des deux conditions n'est pas valide")
```
 Dans ce premier script, la fonction print affichera la string "Les conditions sont valides". Cette string est affichées car les deux conditions sont valides. La première condition i <  5 est effectivement valide, la seconde condition isTrue == True est aussi valide.
 
 ```python
isFalse = False
i = 0
if(i < 5 and isFalse == True):
    print('Les conditions sont valides')
else:
    print("L'une des deux conditions n'est pas valide")
```

Dans le second script, la fonction print n'affichera pas la string "Les conditions sont valides". Car l'une des deux conditions n'est pas valide. La premiere condition i <  5 est valide, la seconde condition isFalse == True n'est pas valide.

On voit ici que le mot clef and renverra 'True' si et seulement si les deux conditions sont valides.

### OR

Le second mot clef utilisé est or. "or" renvoit 'True' si au moins l'une des deux conditions est valide.

```python
isTrue = True
i = 0
if(i < 5 or isTrue == True):
    print('Les conditions sont valides')
else:
    print('Les conditions ne sont pas valides')
```

Dans le premier script, les deux conditions sont valides et la fonction print affiche 'Les conditions sont valides'.

```python
isFalse = False
i = 0
if(i < 5 or isFalse == True):
    print('Les conditions sont valides')
else:
    print('Les conditions ne sont pas valides')
```

Dans ce second script, l'une des deux conditions est valide donc le bloc d'instruction est lu et la fonction print affiche 'Les conditions sont valides'.. 

```python
isFalse = True
i = 5
if(i < 5 or isFalse == True):
    print('Les conditions sont valides')
else:
    print('Les conditions ne sont pas valides')
```

Dans ce troisième script, encore une fois l'une des deux conditions est valide donc le bloc d'instruction est lu et la fonction print affiche 'Les conditions sont valides'.. 

```python
isTrue = False
i = 31
if(i < 5 or isTrue == True):
    print('Les conditions sont valides')
else:
    print('Les conditions ne sont pas valides')
```

Dans le quatrième script, les deux conditions ne sont pas valides donc nous n'entrons pas dans le bloc d'instruction if et le bloc d'instruction  else est lu. La fonction print affiche 'Les conditions ne sont pas valides'.

### Déclarer un tableau

Un tableau est une variable contenant plusieurs valeurs.
Tous les types de variables peuvent etre contenus dans un tableau : int, float, string, boolean

On declare le tableau comme suivant
```python
      nomDuTableau = [valeur1, valeur2]
 ```  
 
Il n'y a pas de limite pour le nombre de valeurs stockées.

### Récupérer les valeurs du tableau

Pour récupèrer une valeur stockée dans un tableau, on indique le nom du tableau et l'index de la valeur que l'on souhaite récupérer. L'index est la position de la valeur dans le tableau. Le premier index n'est pas égal à 1 mais à 0. L'index doit être indiqué entre crochets.

```python
     >>> nomDuTableau[index]
        valeurStockeeALIndex
 ```  

### Exemple

```python
    >>>  ConstructeurAutomobile = ['peugeot', 'Renault', 'Mercedes','Volkwagen', 'GM', 'Toyota']
    >>> ConstructeurAutomobile[0]
    'peugeot'
    >>> ConstructeurAutomobile[1]
    'Renault'
    >>> ConstructeurAutomobile[2]
    'Mercedes'
    >>> ConstructeurAutomobile[3]
    'Volkwagen'
    >>> ConstructeurAutomobile[4]
    'GM'
    >>> ConstructeurAutomobile[5]
    'Toyota'
 ```  

### Le cas des Strings

D'une certaine manière les strings sont des tableaux. Des tableaux qui à chaque index stockent un unique caractère.
Ainsi on peut utiliser l'annotation des tableaux avec une string pour récupérer le caractère stocké à l'index.

```python
    >>>  Noel = 'santa'
    >>> Noel[0]
    's'
    >>> Noel[1]
    'a'
    >>> Noel[2]
    'n'
    >>> Noel[3]
    't'
    >>> Noel[4]
    'a'
 ``` 
 
## La boucle non bornée : while

La boucle while permet de répéter un bloc d'instructions tant qu'une condition est valide.

```python
while (condition):
    ## Bloc d'instructions
 ```  
 
### Exemple

```python
    i = 0
    while (i < 3):
        print(i)
        i = i + 1
```
 
Ici on déclare i qui vaut 0 en premier lieu. Ensuite on indique la boucle while avec la condition à valider. Enfin on écrit le bloc d'instruction où l'on affiche la valeur de i et où l'on ajoute 1 à i.

### Boucle infinie

#### Définition

Une boucle infinie est une boucle qui sera éxécuté à tout jamais; pour toujours. On obtient une boucle infinie lorsque sa condition est toujours valide. Dans les jeux vidéo ce sont des boucles infinies qui font clignoter les objets.

![pacman gif](https://media.giphy.com/media/56wWg3WBNYexW/giphy.gif)

#### Exemple

```python
while True:
 print('1')
```

```python
i = 1
while i < 5:
 print('1')
```


## Supplément fonction

### return avec plusieur valeurs.

Nous avons vu que l'instruction return peut renvoyer une valeur. Mais il faut savoir que l'on peut aussi retourner plus d'une valeur.

Ainsi le code:

```python
    def doubleEtTriple(arg1):
        return(arg1 * 2, arg1 * 3)
    x, y = function(2)
 ```

donne:

```python
    >>> print(x, y)
    4, 6
 ```

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/18){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}

