---
title: "Python - While : Exercices supplémentaires"
categories:
  - exercices
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Fondamentaux_Python3_while-additional_Exercices.png"
image_alt: exercices_supplémentaires_python3
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/while-exercices-supplementaires
show_comments: true
---

<!-- more -->

{% include header_python.html %}

## Exercices

1. créez une fonction fibo() sans paramètre. Qui demande à l'utilisateur "Quelle est l'index du nombre que vous cherchez dans la suite de fibonacci?". La fonction vous renvoie et affiche le nombre demandé. Exemple:

```python
>>>Quelle est l'index du nombre que vous cherchez dans la suite de fibonacci? 7
>>>13
```
2. Créez une fonction isPrime() qui prend en parametre un int. Cettre fonction vous renvoie True si le nombre est premier. et False si le nombre n'est pas premier.
3. Créez une fonction hundredPrime() qui affiche les cent premiers nombres premiers. 

4. La Conjecture(s) de Goldbach.
	Objectifs : étudier deux conjectures de Goldbach. Une conjecture est un énoncé que l’on pense vrai mais que l’on ne sait pas démontrer. 
	1. La bonne conjecture de Goldbach :

Tout entier pair plus grand que 4 est la somme de deux nombres premiers. Par exemple 4 = 2 + 2, 6 = 3 + 3, 8 = 3 + 5, 10 = 3 + 7 (mais aussi 10 = 5 + 5), 12 = 5 + 7, ...  Pour n = 100 il y a 6 solutions : 100 = 3 + 97 = 11 + 89 = 17 + 83 = 29 + 71 = 41 + 59 = 47 + 53. Personne ne sait démontrer cette conjecture, mais tu vas voir qu’il y a de bonnes raisons de penser qu’elle est vraie. 

	
- Programme une fonction nombre_solutions_goldbach(n) qui pour un entier pair n donné, trouve combien il existe de décompositions n = p + q avec p et q deux nombres premiers et p <= q. Par exemple pour n = 8, il n’y a qu’une seule solution 8 = 3 + 5, par contre pour n = 10 il y a deux solutions 10 = 3 + 7 et 10 = 5 + 5. 
	- Indications:
		- Il faut donc tester tous les p compris 2 et n / 2 ;
		- poser q = n − p;
		- on a une solution quand p <= q et que p et q sont tous les deux des nombres premiers.
- Prouve avec la machine que la conjecture de Goldbach est vérifiée pour tous les entiers n pairs compris entre 4 et 10 000.
	
	2. La mauvaise conjecture de Goldbach :

Tout entier impair n peut s’écrire sous la forme n = p + 2 k ^ 2 où p est un nombre premier et k un entier (éventuellement nul).
- Programme une fonction existe_decomposition_goldbach(n) qui renvoie « vrai » lorsqu’il existe une décomposition de la forme n = p + 2 k ^ 2.
- Montre que cette seconde conjecture de Goldbach est fausse ! Il existe deux entiers plus petits que 10 000 qui n’admettent pas une telle décomposition. Trouve-les !

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [Conjecture de Goldbach](http://exo7.emath.fr/cours/livre-python1.pdf){:target="_blank"}

