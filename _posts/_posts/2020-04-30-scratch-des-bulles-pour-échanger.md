---
title: "Scratch - Des bulles pour échanger"
categories:
  - cours
  - scratch
  - fondamentaux
  - école et collège
image: "/images/formations/scratch-cours3.jpg"
author_staff_member: axepromotion
permalink: /fondamentaux-scratch/technique-du-récit
---

<!-- more -->

Ce cours s’inscrit dans le cadre de [notre formation gratuite à distance à scratch](https://codedusoir.org/formations/twitch-scratch3/).
Nous voulons te faire découvrir la programmation en s’amusant et en renforçant tes connaissances en mathématiques, en français, en anglais... 

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"}.

## Le développement cognitif selon Jean PIAGET

Pour t'aider à comprendre les différent stades de l'apprentissage, je te présente d'abord la théorie de Jean PIAGET, puis la théorie néo-Piagétiennes

Jean PIAGET (1896-1980) est un psychologue Suisse passionné de sciences naturelles, son œuvre est centrée sur le développement cognitif, théorie opératoire de l'intelligence, et sur l'épistémologie génétique, théorie générale de la genèse des connaissances, applicable au monde du vivant.

Jean Piaget distingue 4 stades de l'intelligence :

- De 0 à 2 ans : stade sensorimoteur. Alors,  le contact qu’entretient l’enfant avec le monde qui l’entoure dépend entièrement des mouvements qu’il fait et des sensations qu’il éprouve.

- De 2 à 7 ans : période pré-opératoire. Là, l’enfant demeure beaucoup orienté vers le présent et les situations physiques concrètes, ayant de la difficulté à manipuler des concepts abstraits. Sa pensée est aussi très égocentrique.

- De 7 à 11 ans : stade des opérations concrètes.  Avec l’expérience l’enfant devient capable d’envisager des événements qui surviennent en dehors de sa propre vie. Il commence aussi à conceptualiser et à créer des raisonnements logiques nécessitant cependant un rapport direct au concret. 

- A partir de 11 ans, stade des opérations formelles. Les nouvelles capacités de ce stade, comme celle de faire des raisonnements hypothético-déductifs et d’établir des relations abstraites, sont généralement maîtrisées autour de l’âge de 15 ans. À la fin de ce stade, l’adolescent peut donc, comme l’adulte, utiliser une logique formelle et abstraite. Il peut aussi se mettre à réfléchir sur des probabilités et sur des questions morales come la justice.

## La théorie néo-piagétienne

La théorie néo-piagétienne s'est développée depuis les années 1970-1980. Dans le néo-piagisme les âges sont supprimés, c'est-à-dire que lorsque vous commencez à apprendre une nouvelle compétence, vous revenez au niveau le plus bas. Ainsi, même si vous êtes déjà un adulte et que vous êtes tout à fait capable de faire un raisonnement opérationnel formel sur le langage ou les mathématiques, quand vous apprenez la programmation, vous revenez au niveau le plus bas. Ne vous inquiétez donc pas, cela prend juste un peu de temps. Vous serez dans ces phases basses pendant un certain temps, et ensuite vous serez capable de faire du raisonnement logique dans la programmation.

## L'introduction de la programmation auprès des enfants

C'est Seymour PAPERT, Informaticien, mathématicien et éducateur, qui a introduit la programmation auprès des enfants en 1967. 
Il a acquis une grande renommée pour l'intérêt qu'il a accordé à l'impact des nouvelles technologies sur l'apprentissage en général et les établissements d'enseignement en particulier. À cette fin, Papert utilisa les travaux de Piaget pour développer au MIT le langage de programmation Logo. Logo était conçu comme un outil destiné à améliorer chez les enfants la manière de penser et de résoudre les problèmes. Il est aussi l'un des principaux acteurs du projet « Un ordinateur portable par enfant.

## Rappels

Nous rappelons que scratch est un véritable langage de programmation. Il permet d’acquérir les connaissances nécessaires à la programmation grâce aux blocs qu’il propose, et dans lesquels nous pouvons écrire dans notre langue maternelle. 
Tu peux aussi, à ta convenance, télécharger des lutins, de la musique, des enregistrements de ta voix… 
On peut, en enseignant la programmation avec scratch, y associer d’autres matières comme les mathématiques, le français… 
Scratch nous apprend aussi à gérer le temps et à réajuster nos erreurs.

## Pratique

- D'abord, tu te rends dans ton compte scratch [à cette adresse](https://scratch.mit.edu/){:target="_blank"}.
- Tu vas également sur notre forum pour poser tes questions ou avoir accès à des liens, ou à d'autres ressources [à cette adresse](https://codedusoir.zulipchat.com){:target="_blank"}. C'est là que tu pourras discuter avec nous et demander de l'aide si tu as besoin.

Dans le 3ème cours, nous utilisons la technique du récit pour permettre aux lutins de mener une conversation. 
Scratch et Butterfly se rencontrent. 
Butterfly aimerait aller se promener avec scratch mais c’est impossible parce que scratch a promis à Horse d’aller coder avec lui…
Et scratch tient toujours ses engagements.

Nous avons utilisé les blocs jaunes "évènement", les blocs violets "apparence", les blocs orange "contrôle" et les blocs bleus "mouvement"

Peux-tu créer un programme, à partir d'une histoire que tu auras créée, en t'aidant des images suivantes :

![](/images/formations/Programme-butterfly-3.jpg)

![](/images/formations/Programme-scratch-3.jpg)

![](/images/formations/Programme-horse-3.jpg)

Tu peux aussi utiliser ta voix pour faire parler tes lutins. Tu sais le faire ;-)

N'oublie pas que tu peux demander de l'aide sur le forum

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"} en direct.

Tu m'enverras un message sur notre forum pour me donner ton ressenti. Je suis très intéressée 
N'oublie pas, si tu as besoin d'un coup de main, nous sommes là pour t'aider.

Merci d'avoir lu ce cours.

## Questionnaire 
Evalue tes connaissances et visualise ta progression [ici](https://form.codedusoir.org/node/17){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles

- [forum](https://codedusoir.zulipchat.com){:target="_blank"}
- [twitch](https://www.twitch.tv/codedusoir/){:target="_blank"}
- [edx](https://courses.edx.org/login?next=/courses/course-v1%3ADelftX%2BScratchTENGX%2B1T2019/courseware/e0ad11486de74ade95bbc327bfe84d7c/2717a2fdaeb94fa1bae1b4674ab55cde/%3Factivate_block_id%3Dblock-v1%253ADelftX%252BScratchTENGX%252B1T2019%252Btype%2540sequential%252Bblock%25402717a2fdaeb94fa1bae1b4674ab55cde){:target="_blank"}
