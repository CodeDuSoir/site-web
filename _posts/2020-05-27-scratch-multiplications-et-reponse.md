---
title: "Scratch - Multiplications et réponse"
categories:
  - cours
  - scratch
  - fondamentaux
  - école et collège
image: "/images/formations/scratch-cours7.png"
author_staff_member: axepromotion
permalink: /fondamentaux-scratch/multiplications-et-reponse
---

<!-- more -->


## Multiplications

Il est temps de mettre au point des programmes pour réviser les tables de multiplication. C'est une programmation que je veux faire avec toi. 
Tu vas pouvoir aficher les résultats de la table de ton choix.
Donc, au lieu de te demander ton nom, nous te demanderons **"quelle table aimeriez-vous voir ?"**, et ce serait un peu étrange si je dis "quelle table aimeriez-vous voir ?", puis que l'on réponde cinq par exemple et que l'on dise ensuite "bonjour cinq". 
Donc... nous allons adapter cela avec **"vous choisissez la table de"** et ensuite **"réponse"**. 
Essayons donc : **"Quelle table aimeriez-vous voir"**, **"réponse"**, (tu as choisi la table de sept). 
Je dois penser à ajouter l'espace pour que ce soit plus facilement lisible : **"quelle table aimeriez-vous voir ?", puis **"Vous choisissez la table de **, **"réponse"**. 

Après nous pouvons dire **"mettez ma variable à..."**...**"réponse"**.
Tu veux voir ce qu’il se passe avec la table des sept ? 
Et voilà, tu obtiens la table des sept en entier...
C'est un concept assez délicat... 
C’est un niveau de programmation supérieur... Tu dois penser de manière tactique aux variables qui doivent être utilisées... 

![](/images/formations/scratch-cours7-ex1.png)

## Révision de toutes les tables de multiplications

Pour réviser toutes les tables, il va falloir que tu utilises : 
- les variables comme précédemment, tu vas aussi créer des variables "a" et "b" et "score"
- les opérateurs avec ''les nombres aléatoires entre...et ...", avec "*" pour multiplier, avec "=", avec ""regrouper...et...".
- les conditions : "si...alors...Sinon
- les capteurs comme ''demander...et attendre,
- le bloc "dire...pendant...3
- les boucles pour répéter 10 fois, par exemple,

Cela demande un peu de pratique... C'est pourquoi je te demande de recommencer l'exercice.

![](/images/formations/scratch-cours7-ex2.png)

Garder toujours en tête que:

- Un code dont on est l'auteur peut être difficile à relire si on l'abandonne quelque temps.
- Lire le code d'un autre développeur est toujours plus délicat.
- Si votre code doit être utilisé par d'autres, il doit être facile à reprendre (à lire et à comprendre).

Et dans les prochains cours, nous essaierons de créer un quiz.


Merci d'avoir lu ce cours. À très vite pour les quiz

## Questionnaire 

Evalue tes connaissances et visualise ta progression [ici](https://form.codedusoir.org/node/21){:target="_blank"}.
Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
