---
title: Python - Fonctions
categories:
  - cours
  - python
  - fondamentaux
  - lycée
image: "/images/formations/fonctions-python3.png"
video_url: "https://bittube.video/videos/embed/be828db8-83b1-4643-83ba-b280636473fe"
image_alt: fonctions pythons
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/fonctions
show_comments: true
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## Fonctions

### Définition

En informatique, une fonction est une séquence d'instructions réalisant une certaine tâche. Cette séquence d'instructions est encapsulée dans la fonction et ne sera lancée qu'à l'appel de la fonction.

Une fonction est un peu un script lancé par un autre script.

### Utilisation

```python
def fonc(argument_1, argument_2, argument_3):
    Bloc d'instruction
```

On a le mot-clé 'def' qui indique à Python que l'on va définir une fonction. On a le nom de la fonction, ici "fonc" qui nous servira a faire l'appel de la fonction. Puis entre les parenthèses ouvrante et fermante on a la déclaration des arguments qui seront fournis lors d'un appel à la fonction. Les arguments sont séparés par des virgules. Les parenthèses sont obligatoires, quand bien même votre fonction n'attendrait aucun argument.

Les arguments ou paramètres sont les valeurs entrées dans la fonction et qui seront traitées par la fonction. 

Enfin, indenté, on a le Bloc d'instruction ou corps de la fonction.

### Exemple

```python
def greetings(name):
    print("Bonjour " + name)
```
Ici on a créé la fonction greetings() qui a en paramètre name et qui, lancée et appelée, exécute l'instruction print('Bonjour ' + name).

### L'appel à la fonction

Après avoir lancé la fonction greetings() via le bouton 'run' de repl.it, il ne se passe rien... En effet il nous manque une dernière étape! Il faut maintenant appeler la fonction. On peut appeler la fonction dans la console ou dans le script. Voyons d'abord comment appeler la fonction dans la console:

Il faut simplement écrire la fonction avec le paramètre demandé:

```python
>>> greetings('Quentin')
    'Bonjour Quentin'
```
 
 Comme vous le voyez on appelle la fonction en écrivant son nom et en indiquant pour l'argument name une valeur (ici 'Quentin').
 
 Pour appeler une fonction dans la partie script de repl.it, on écrit à la suite de la définition de la fonction l'appel, on a donc:
 
 ```python
def greetings(name):
    print("Bonjour " + name)
greetings('Quentin')
```
 
Là, dans la console vous verrez écrit "Bonjour Quentin".

Attention: Vous ne pouvez pas faire l'appel avant d'avoir définit votre fonction. Autrement lorsque vous écrivez ceci: 
```python
greetings('quentin')

def greetings(name):
  print("Bonjour " + name)
```
vous recevrez ce message d'erreur:
```python
"Traceback (most recent call last):
  File "main.py", line 3, in <module>
    greetings('quentin')
NameError: name 'greetings' is not defined
```

### Attribution de valeurs aux paramètres

#### Attribuer des valeurs par défaut
vous pouvez attribuer des valeurs par défaut aux paramètres:

```python
def printValue(x = 1, y = 2, z = 3):
    print(x, y, z)
```
Si l'on appelle la fonction sans indiquer de valeurs aux arguments dans la console:

```python
>>>printValue()
1, 2, 3
```

Python décide d'utiliser les valeurs par défaut déjà mentionnées.
On peut aussi indiquer qu'une partie des valeurs, alors Python remplacera les valeurs par défaut par celle indiquées par l'utilisateur:

```python
>>>printValue(0, 1, 2)
0, 1, 2
>>>printValue(x = 13)
13, 2, 3
>>>printValue(y = 4, x = 13)
13, 4, 3
```

#### L'instruction return

L'instruction return signifie qu'on va renvoyer la valeur contenu dans les parenthèse de l'instruction. Cette instruction arrête le déroulement de la fonction, le code situé après le return ne s'exécutera pas. Par exemple, la valeur renvoyée par la fonction peut être stockée dans une variable.

```python
def greetings(name):
    print("Bonjour " + name)
    return "Nous avons salué " + name

salutation = greetings('quentin')
```
Dans la console après avoir lancé ce code on aura:
```python
"Bonjour quentin"
 >>> salutation   
    "Nous avons salué quentin"
```
 

```python
salutation = greetings('quentin')
```

Cette dernière ligne appelle la fonction greetings et stocke la valeur retournée dans la variable salutation. En appellant la fonction, on a lancé le bloc d'instruction, donc la string "Bonjour" + name a été affichée(printé en franglais).

Attention afficher et renvoyer ne veut pas dire la même chose. 
La valeur affiché est écrite dans la console. Quant à la valeur renvoyé elle est éjectée par la fonction à la fin de celle-ci et elle peut donc être stockée à l'extérieur de la fonction.
```

{% include footer_python.html %}

Merci d'avoir lu ce cours. La suite ! Les listes !

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/8){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
