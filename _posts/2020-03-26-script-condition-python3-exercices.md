---
title: Python - Script et condition
categories:
  - exercices
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Script-condition-python3_exercices.png"
image_alt: script_condition_exercices_python3
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/script-conditions-exercices
show_comments: true
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## Exercices

#### Les scripts

- Créez un script qui affiche la string “Hello world”
- Dans la partie script de repl.it, déclarez une variable x et attribuez lui la valeur 10. Puis multipliez la variable par 3. Enfin utilisez la fonction print() pour afficher la valeur de x. Puis lancez le script.
- Dans la partie script de repl.it, déclarez une variable greetings et attribuez lui la valeur "Bonjour ". Déclarez une variable name et attribuez lui la string contenant votre nom. Puis concaténez les variables greetings et name et stockez le resultat dans la variable sentence. Enfin utilisez la fonction print() pour afficher la valeur de sentence. Lancez le script.


#### La manipulation des chaînes de caractères.
- Dans la partie script de repl.it, créez une variable x qui contient le nombre de votre choix. Créez un programme qui affiche la string “x est un type float” si le type de x est float, la string “x est un type int” si le type de x est int, si x est un type string le programme vous renvoie la valeur de x. Lancez le script. Testez le avec x = 42 puis x = 4.2 et x = “101010”.
- Dans la partie script de repl.it, créez une variable nombre qui contient le nombre de votre choix. Créez un programme qui vous affiche la string “Je suis un nombre pair” si le nombre est pair, la string “Je suis un nombre impair” si le nombre est impair, et la string “Je suis zero” si le nombre est zero. Lancez le script. Testez le avec nombre = 1 puis nombre = 2 et nombre = 0.

### Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/7){:target="_blank"}. 

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
