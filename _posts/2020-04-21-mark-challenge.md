---
title: "Mark (note)"
categories:
  - challenges
  - python
  - fondamentaux
  - lycée
image: "/images/formations/python-challenge.png"
author_staff_member: adrien-parrot
permalink: /challenges/mark
---

<!-- more -->

{% include header_challenge_python.html %}

# Consignes

- Function name : marks_converter
- Allowed functions : [input()](https://docs.python.org/fr/3/library/functions.html#input), [float()](https://docs.python.org/fr/3/library/functions.html#float) 
- Prerequis : [Variables]({{ site.baseurl }}/fondamentaux-python/operateurs-variables), [Conditions]({{ site.baseurl }}/fondamentaux-python/script-conditions), [Functions]({{ site.baseurl }}/fondamentaux-python/operateurs-variables), [Built-In Functions]({{ site.baseurl }}/fondamentaux-python/fonctions-predefinies)

- Créer un programme python qui converti des notes numériques en lettre allant de A à F sachant que 
  - On obtient A lorsque la note est au minimum 3,7 (inclus)
  - On obtient B lorsque la note est comprise entre 3.7 et 2,7 (inclus)
  - On obtient C lorsque la note est comprise entre 2.7 et 1,7 (inclus)
  - On obtient D lorsque la note est comprise entre 1.7 et 1 (inclus
  - On obtient F lorsque la note est comprise entre 1 et 0 (inclus)

# Aides
Il n'existe pas de note négative !

# Rendu

### Par repl.it (en s'identifiant)

<iframe frameborder="0" width="100%" height="600px" src="https://repl.it/student_embed/assignment/5184021/a057140a7008e0562e637b38e397fc63"></iframe>

Si l'affichage n'est pas correct, voici le [lien d'invitation](https://repl.it/classroom/invite/owOeEyb){:target="_blank"}.

### Par mail (sans s'identifier ) à codedusoir@protonmail.com

Le fichier attendu en pièce jointe doit se nommer main.py et il doit contenir le code suivant
```python
def marks_converter(mark):
  # return a string 
  if mark >= :
    return "une note"
  elif mark >= :
    return "une autre note"
  ...


if __name__ == '__main__':
    try:
        num = float(input("Entrer une note à convertir (entre 0 et 4.0) : "))
        p rint(marks_converter(num))
    except ValueError:
        print("Merci d'entrer un integer")         
```

```python
$> python3 main.py
Entrer une note à convertir (entre 0 et 4.0) : 3.5
B 
```

# On garde le contact

{% include footer_python.html %}

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/) ou contacte-nous sur [forum](http://forum.codedusoir.org){:target="_blank"}.
