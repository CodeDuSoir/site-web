---
title: Python - Fonctions prédéfinies
categories:
  - cours
  - python
  - fondamentaux
  - lycée
image: "/images/formations/fonctions-python3.png"
image_alt: fonctions pythons
author_staff_member: adrien-parrot
permalink: /fondamentaux-python/fonctions-predefinies
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## Affichage : print()

La fonction print() permet d'afficher n'importe quel nombre de valeurs fournies en paramètres. Par défaut, ces valeurs seront séparées les unes des autres par un espace et chaque fonction print se termine par un retour à la ligne.

Il est possible de remplacer le séparateur par défaut (l'espace) par un autre : 
```python
>>> print("Python", "est", "un", "serpent", sep ="!") 
Python!est!un!serpent
```

De même, vous pouvez remplacer le saut à la ligne terminal avec l'argument « end » : 
```python
>>> i = 0
>>> while i < 10: 
...    print(“42”, end ="") 
...    i = i + 1 
...
42424242424242424242
```

## Interaction utilisateur : input()

input() est une fonction qui prend en paramètre une string qui sera affichée dans la console. Cette fonction provoque une interruption dans le programme courant. 
Puis l’utilisateur devra entrer une chaîne de caractères puis appuyer sur la touche entrée.

Avec le script suivant:
```python
value = input("Donnez moi un nombre : “)
print(value)
```

vous aurez dans la console:
```python
    >>>Donnez moi un nombre : 40
    40
```

## Cast : transformation de type
    
Pour transformer un type vers un autre type on utilise des fonctions spécifiques : int(); float(); str()

```python
nombre = 1
print("Le type de nombre est ", type(nombre))
nombreStr = str(nombre)
print("Le type de nombreStr est ", type(nombreStr))
```
La console affiche:
```python
Le type de nombre est int
Le type de nombreStr est str
```

Voyons son application avec la fonction input(). Cette fonction renvoie toujours une chaîne de caractères. 
Si vous souhaitez que l'utilisateur entre un entier, il faut convertir la valeur entrée en une valeur entier avec int() ou float().

```python
>>> string = input("Donnez moi un nombre : ")
Donnez moi un nombre : 40
>>> type(string)
<class 'str'>
>>> nb = int(string)     # conversion de la chaîne en un nombre réel
>>> type(nb)
<class ‘int’>
```

{% include footer_python.html %}

Merci d'avoir lu ce cours.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
