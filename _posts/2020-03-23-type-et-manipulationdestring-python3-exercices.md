---
title: Python - Type et Manipulation de string
categories:
  - cours
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Type-et-manipulationdestring-python3_Exercices.png"
image_alt: type_string_exercices_python3
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/type-manipulation-string-exercices
show_comments: true
---

<!-- more -->

{% include header_python.html %}

## Exercices

#### Le type
- Assignez aux variables x, y, z les valeurs 1, 2.5 et 'string'.
- Affichez le type des variables x, y et z.
- Assignez aux variables i, j, k les valeurs 5, 6.245 et "Manger".
- Quel est le résultat de la comparaison du type de x avec i, j, k?

#### La manipulation des chaînes de caractères.
- Concaténez 'Bonjour' avec "l'ami". Quel est votre résultat?
- Multipliez 'banana' par 4. Quel est votre résultat?
- Assignez aux variables i, j, k les valeurs "J'aime ", 'Je déteste ' et "Manger".
- Concaténez i avec k et j avec k. Quels sont vos résultats?

### Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/4){:target="_blank"}. 

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
