---
title: Python - Listes et boucles while
categories:
  - cours
  - python
  - fondamentaux
  - lycée
image: "/images/formations/table-while-python3.png"
image_alt: Listes_While_python3
video_url: https://bittube.video/videos/embed/67b33a10-c0ae-42eb-a27f-92dee7168e5f
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/liste-while
show_comments: true
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## Listes 

### Déclarer une listes

Une liste est une variable contenant plusieurs valeurs.
Tous les types de variables peuvent etre contenus dans une liste : int, float, string, boolean

On declare la liste comme suivant
```python
      nomDeLaListe = [valeur1, valeur2]
 ```  
 
Il n'y a pas de limite pour le nombre de valeurs stockées.

### Récupérer les valeurs de la liste

Pour récupèrer une valeur stockée dans une liste, on indique le nom de la liste et l'index de la valeur que l'on souhaite récupérer. L'index est la position de la valeur dans la liste. Le premier index n'est pas égal à 1 mais à 0. L'index doit être indiqué entre crochets.

```python
     >>> nomDeLaListe[index]
        valeurStockeeALIndex
 ```  

### Exemple

```python
    >>>  ConstructeurAutomobile = ['peugeot', 'Renault', 'Mercedes','Volkwagen', 'GM', 'Toyota']
    >>> ConstructeurAutomobile[0]
    'peugeot'
    >>> ConstructeurAutomobile[1]
    'Renault'
    >>> ConstructeurAutomobile[2]
    'Mercedes'
    >>> ConstructeurAutomobile[3]
    'Volkwagen'
    >>> ConstructeurAutomobile[4]
    'GM'
    >>> ConstructeurAutomobile[5]
    'Toyota'
 ```  

### Le cas des Strings

D'une certaine manière les strings sont des listes. Des listes qui à chaque index stockent un unique caractère.
Ainsi on peut utiliser l'annotation des listes avec une string pour récupérer le caractère stocké à l'index.

```python
    >>>  Noel = 'santa'
    >>> Noel[0]
    's'
    >>> Noel[1]
    'a'
    >>> Noel[2]
    'n'
    >>> Noel[3]
    't'
    >>> Noel[4]
    'a'
 ``` 
 
## La boucle non bornée : while

La boucle while permet de répéter un bloc d'instructions tant qu'une condition est valide.

```python
while (condition):
    ## Bloc d'instructions
 ```  
 
### Exemple

```python
    i = 0
    while (i < 3):
        print(i)
        i = i + 1
```
 
Ici on déclare i qui vaut 0 en premier lieu. Ensuite on indique la boucle while avec la condition à valider. Enfin on écrit le bloc d'instruction où l'on affiche la valeur de i et où l'on ajoute 1 à i.

### Boucle infinie

#### Définition

Une boucle infinie est une boucle qui sera éxécuté à tout jamais; pour toujours. On obtient une boucle infinie lorsque sa condition est toujours valide. Dans les jeux vidéo ce sont des boucles infinies qui font clignoter les objets.

![pacman gif](https://media.giphy.com/media/56wWg3WBNYexW/giphy.gif)

#### Exemple

```python
while True:
 print('1')
```

```python
i = 1
while i < 5:
 print('1')
```


## Supplément fonction

### return avec plusieur valeurs.

Nous avons vu que l'instruction return peut renvoyer une valeur. Mais il faut savoir que l'on peut aussi retourner plus d'une valeur.

Ainsi le code:

```python
    def doubleEtTriple(arg1):
        return(arg1 * 2, arg1 * 3)
    x, y = function(2)
 ```

donne:

```python
    >>> print(x, y)
    4, 6
 ```

{% include footer_python.html %}

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/9){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}

