---
title: Apprentissage multimodal
categories:
  - Axe Promotion
  - Définition
  - Actualité
image: "/images/formations/apprentissage_multimodal.png"
image_alt: apprentissage_multimodal
author_staff_member: axepromotion
show_comments: true
permalink: /apprentissage-multimodal
---

<!-- more -->

L'objectif de la formation est de toucher le plus grand nombre. Pour cela, élargissons la personnalisation de l'action. 

Pour atteindre l'objectif, comme la pie voleuse, nous allons chercher une combinaison variée de modalités d'apprentissage :
- social learning,
- visioconférence,
- e-learning,
- classe inversée,
- accompagnement, coaching, tutorat...

## Les bénéfices des formations multimodales permettent :
1. D'assimiler les contenus à votre rythme,
2. De discuter, d'échanger, de progresser ensemble,
3. De bénéficier de coaching personnalisé lors d'une séance en présentiel ou en distanciel (visioconférence, téléphone, mail...),
4. Être accompagné dans la durée.

## Les  étapes du parcours de formation multimodale :
1. Présenter la formation : thème, objectifs, méthode, parcours, prérequis, évaluation, certification,
2. Évaluer les connaissances : quizz pour identifier les objectifs et le niveau de connaissance des participants. Le début de la formation est construit sur la connaissance initiale des stagiaires. Les réponses servent de support à un échange sur des situations et des expériences professionnelles rencontrées par les stagiaires présents.
3. Faire connaissance : Social Learning (fini les tours de table sans fin !). Création d'une communauté d'apprenants,
4. Démarrer le e-learning de la formation : seul mais pas isolé. SPOC (Small Private Online Course) ou modules de 5 à 8 minutes (+ pour certaines formations). La communauté des apprenants encadrée par le formateur coach s'entraide,
5. Évaluer les acquis : avant de passer au module suivant, un quizz récapitule les connaissances acquises,
6. Intervenir auprès des apprenants : en présentiel (regroupement...) ou en distanciel (mail, téléphone, visioconférence...), le formateur coach intervient en motivant, en régulant en conseillant dans la mise en application du sujet étudié. 
7. Mettre en application : l'apprenant met en application les connaissances acquises pendant la formation,
8. Débriefer : questions et réponses avec le formateur coach suite à la mise en application,
9. Valider des acquis : CPF (Compte Personnel de Formation), OPCO (OPérateur de COmpétences)... Les statistiques de la plateforme de e-learning certifient la durée d'apprentissage et l'implication de l'apprenant.

## Sources
- [axepromotion.com/post/formation-multimodale](https://www.axepromotion.com/post/formation-multimodale){:target="_blank"}
