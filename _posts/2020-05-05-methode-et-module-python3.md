---
title: "Python - Méthode et Module"
categories:
  - cours
  - python
image: "/images/formations/Fondamentaux_Python-method_attributes_moduls.png"
image_alt: Table_While_python3
author_staff_member: quentin-parrot
show_comments: true
---

<!-- more -->
Ce cours s'inscrit dans le cadre de notre [formation gratuite à distance à Python](https://www.codedusoir.org/formations/twitch/).

Nous voulons te faire découvrir la programmation en s’amusant et en renforcant tes connaissances en mathématiques.
Retrouve nous les mardis et vendredis à 16h sur [twitch](https://stream.codedusoir.org){:target="_blank"} en direct.

# Les Objets et les méthodes

Nous avons vu plusieurs types de variables :

- les listes
- les float
- les string

Toutes ces variables ou structures de données sont des "**objets** Python". Chaque objet a un type qui permet à Python de comprendre l'objet créé. De plus, chaque objet est créé avec des **méthodes**. En simplifiant, une méthode est une fonction rattachées à son objet.

Prenons une analogie. Les objets peuvent être assimilés à des objets de la vie quotidienne. Une voiture, un vélo, une moto. Chaque objet a des capacités et des fonctionnalités différentes. 
Une voiture, un vélo ou une moto peuvent tous rouler. Ils auront donc tous la méthode (fonction) rouler(). Par contre seule la voiture peut "fermer les 4 portes à clé". Elle est donc la seul à avoir la méthode fermer_les_4_portes_a_clé().
En python chaque objet aura  des méthodes appropriée et spécifique.

On utilise une méthode d'un objet (on dit aussi appeler une méthode) en écrivant le nom de la variable suivi d'un ".", du nom de la méthode et des parenthèses() à l'intérieur desquelles peut se trouver des paramètres:

```python
>>> string = 'coucou'
>>> string.upper()
'COUCOU'
>>>  string.count('I')
0
```

Ici on a utilisé deux méthodes attachées au type string (on parle aussi de **classe** : la classe string), l'une sans paramètre et l'autre avec.

Attention, chaque type de variable a des méthodes différentes. Leurs noms peuvent varier, parfois certain type ont des méthodes homonymes qui fonctionnent différemment.


```python
>>> nombre = 145
>>> nombre.upper()
AttributeError: 'int' object has no attribute 'upper'
>>> nombre.count('I')
AttributeError: 'int' object has no attribute 'count'
>>> print(nombre.bit_length())
8
```

Les deux premières méthodes vous renverront des erreurs. Cependant la dernière(appelée dans le print) vous renverra 8. Alors qu'utilisée avec une string, la méthode bit_length vous renverra une erreur.

En somme, les méthodes sont des fonctions, créés par d'autres développeurs, qui vous permet de résoudre vos problèmes.

# Too Much, Too Much methods!

On a vu de nombreuses méthodes, pourtant ce n'est qu'une goutte d'eau dans l'immensité que représente l'ensemble des méthodes disponibles avec python. 
Voici par exemple dans la documentation officielle de python, les méthodes liées aux string : https://www.programiz.com/python-programming/methods/string/capitalize.
Après avoir lu cette pag, vous pouvez commencer à vous poser quelques questions. Comment maintient-on à jour tout ce code ? Comment fait-on pour que python ne 'confonde' pas des parties de code avec d'autre ? Comment les développeurs ne se perdent pas dans cet océan de méthodes ? 

Les développeurs ont trouvé une solution : les **paquets** ou packages en anglais.

Les paquets sont des dossiers avec des fichiers remplient de code (ou scripts) Python. Dans ces fichiers se trouvent des objets spécifiques avec des méthodes spécifiques.

Ces fichiers de code sont aussi appelés **modules**.

Pour résumé les modules sont compris dans des paquets.

Il faut savoir que des milliers de paquets existent. Voici quelques exemples:

- Le module math pour utiliser des fonction de mathématiques (cos, sin, ...)
- Le module random est un module qui regroupe des fonctions permettant de simuler le hasard.
- Le paquet numpy pour les data sciences.
- Le paquet Matplotlib pour les data vizualisation.
- Le paquet scikit-learn pour le machine learning. 

L'utilisation des paquets permet d'alléger le code et de le compartimenter pour faciliter la maintenance et la compatibilité sans saturer Python de code superflu.

En Python, il faut distinguer deux grandes catégories de modules/paquets  :
- ceux *standards* qui ne font pas partie du langage en soi mais sont intégrés automatiquement par Python. On n'a pas besoin de les installer.
- ceux *développés* par des développeurs externes qu’on va pouvoir utiliser. Pour installer un paquet sur votre machine vous pouvez utiliser pip : le système de maintenance des paquets pour python.

aller sur https://pip.pypa.io/en/stable/installing/

```shell
download get-pip.py
python3 get-pip.py
pip3 install numpy
```

## Utiliser les paquets

Après installation du paquet on doit importer le module que l'on souhaite utiliser.

On peut importer soit le paquet entier soit un module spécifique.

Voici comment on importe un paquet dans python :

```python
import numpy
numpy.array([])
```
On peut aussi utiliser un **alias** (= renommer) pour faciliter l'écriture des modules dans notre script : 

```python
import numpy as np
np.array([])
```
Ici on a une simplification, qui rend l'écriture du code plus rapide.

On importe un module spécifique d'un paquet comme cela :

```python
from numpy import array
array([])
```

C'est une manière de faire très simple mais on perd le contexte du module. Si un autre développeur commence à lire notre code le risque est qu'il ne sache pas ce qu'est array([]) tandis que np.array([]) permettra à votre lecteur et vous même de faire rapidement le lien entre array et le paquet numpy.

### Les attributs

Nous avons vu que les fonctions rattachées à un object sont appelées "méthodes". Une variable rattachée à un objet est appelée "attributs de données".

Comme un attributs de données est propre à un objet, on doit, pour accéder à la valeur de l'attribut, d'abord préciser le nom de la variable suivi d'un "." et du nom de l'attribut.

Pour reprendre l'exemple de la voiture, cité plus haut, on a :

```python
voiture.fermer_les_4_portes_a_clé()
"La voiture a ces 4 portes fermées"
voiture.nombre_de_roues
4
```

Les modules ont aussi des attributs :

```python
import Math

print(math.pi)
3.141592653589793
```

On sait que lorsqu'on tente d’accéder à la méthode d'un module sans indiquer le nom du module, on reçoit un message d'erreur. C'est pareil avec les attributs de donnée d’un objet. Si on ne stipule pas le nom de l'objet, Python renverra une erreur puisque les attributs de données sont propres à l'objet. 

```python
import Math

print(pi)
Traceback (most recent call last):
  File "main.py", line 3, in <module>
    print(pi)
NameError: name 'pi' is not defined
```

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/14){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
- [pierre-giraud](https://www.pierre-giraud.com/python-apprendre-programmer-cours/oriente-objet-classe-attribut/){:target="_blank"}
