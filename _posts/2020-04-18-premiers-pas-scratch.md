---
title: "Scratch - Premiers pas"
categories:
  - cours
  - scratch
  - fondamentaux
  - école et collège
image: "/images/formations/scratch-cours1.jpg"
author_staff_member: axepromotion
permalink: /fondamentaux-scratch/premiers-pas
---

<!-- more -->

Ce cours s’inscrit dans le cadre de [notre formation gratuite à distance à scratch](https://codedusoir.org/formations/twitch-scratch3/).
Nous voulons te faire découvrir la programmation en s’amusant et en renforçant tes connaissances en mathématiques, en français, en anglais... 

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"}.

## Préparation 

- D'abord, tu crées ton compte sur scratch en te rendant [à cette adresse](https://scratch.mit.edu/){:target="_blank"}.

- Puis tu crées ton compte sur notre forum en te rendant [à cette adresse](https://codedusoir.zulipchat.com){:target="_blank"}. C'est là que tu pourras discuter avec nous et demander de l'aide si tu as besoin.

## Découverte de l'interface

Avant de découvrir l'interface, prends 5 minutes pour remplir le [questionnaire](https://form.codedusoir.org/node/13) 
Il te permettra de savoir si tu as déjà acquis les connaissances qui seront délivrées dans ce cours. 
Il te permettra aussi de visualiser ta progression!

Maintenant que tu as rempli le questionnaire nous allons découvrir l'interface de scratch.

Je me rends donc sur scratch et je me connecte.
A droite du logo ```scratch```, on va voir comment tu choisis la langue que tu vas utiliser : ta langue maternelle de préférence. On va voir aussi que l'onglet ```fichier``` te permet de sauvegarder ou de télécharger tes projets, et surtout, de créer un nouveau projet.
 
## Premier pas dans scratch

- Dans l'onglet ```fichier```, tu vas cliquer sur ```nouveau``` et dans la petite fenêtre à droite, là où tu vois ```entitled``` (ça veut dire intitulé en français), tu vas donner un nom à ton projet (par exemple : mon premier projet).
- Maintenant tu peux faire connaissance avec scratch, le chat.
- Sur la scène, là où se trouve scratch tu vas installer l'arrière-plan ou le décor et tu vas pouvoir supprimer scratch pour ajouter un autre lutin (sprite en anglais. Les décors et lutins sont classés par ordre alphabétique (a,b,c,d...). Il te faut cliquer dans le cercle dans lequel se trouve la tête de lutin,  en bas de la scène, pour retrouver les lutins et dans le cercle dans lequel se trouve des montagnes,  en bas de la scène, pour retrouver les décors. 
- Ensemble nous allons déplacer, agrandir et diminuer la taille des lutins,
- Pour terminer tu vas découvrir l'onglet ```code``` en haut à gauche, là où se trouve la palette des blocs de plusieurs couleurs pour gérer les mouvements, l’apparence, le son et plein d’autres choses. Tu as vu également la zone de script là où on déposera les blocs pour créer ton programme.

## Et après...

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"} en direct.
Mais en attendant, je te demande de créer et d'intituler ton premier projet. 
Tu m'enverras un message sur notre forum pour me dire quel personnage et quel arrière-plan tu as choisis.
N'oublie pas, si tu as besoin d'un coup de main, nous sommes là pour t'aider.

Merci d'avoir lu ce cours.

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/13){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles

- [forum](https://codedusoir.zulipchat.com){:target="_blank"}
- [twitch](https://www.twitch.tv/codedusoir/){:target="_blank"}
- [edx](https://courses.edx.org/login?next=/courses/course-v1%3ADelftX%2BScratchTENGX%2B1T2019/courseware/e0ad11486de74ade95bbc327bfe84d7c/2717a2fdaeb94fa1bae1b4674ab55cde/%3Factivate_block_id%3Dblock-v1%253ADelftX%252BScratchTENGX%252B1T2019%252Btype%2540sequential%252Bblock%25402717a2fdaeb94fa1bae1b4674ab55cde){:target="_blank"}

