---
title: Python - Type et Manipulation de string
categories:
  - cours
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Type-et-manipulationdestring-python3.png"
image_alt: type_string_cours_python3
video_url: https://bittube.video/videos/embed/91fb1bb2-1ea8-4a43-9c35-109e8b8f5084
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/type-manipulation-string
show_comments: true
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## TYPE

### Définition

Le type est le groupe de la classe dans laquelle appartient une valeur. Il définit la nature des valeurs que peut prendre une donnée, ainsi que les opérateurs qui peuvent lui être appliqués. Les types sont aux valeurs ce que les classes animales sont aux animaux. Un humain est un mammifère. On peut donc dire que les individus ont des comportements et des fonctionnements spécifiques.

Il existe avec python les types de valeur suivants: int pour les nombres entiers  de 32 ou 64 bits, float pour les nombres décimaux, string pour les chaînes de caractères, long pour les nombres qui ont besoin de plus de 32 bits.

### Les bits et les bytes

Un bit est l'unité la plus simple dans un système de numération, ne pouvant prendre que deux valeurs désignées le plus souvent par les chiffres 0 et 1. Un bit ou élément binaire peut représenter aussi bien une alternative logique, exprimée par faux et vrai, qu'un chiffre du système binaire.

Dans la théorie de l'information, un bit est la quantité minimale d'information transmise par un message, et constitue à ce titre l'unité de mesure de base de l'information en informatique. 



Le byte ou octet est une suite de 8 bits.

### La fonction type()

Pour trouvez le type on peut utiliser la fonction type(). Nous verrons ce qu'est une fonction dans une séance future.

Par exemple on peut avoir le type du nombre trois en écrivant:

```python
>>> type(3)
<class 'int'>
```

On peut avoir le type du nombre trois et demi en écrivant:

```python
>>> type(3.5)
<class 'float'>
```

On peut demander le type d'une opération en écrivant:

```python
>>> type(3 * 5)
<class 'int'>
```

```python
>>> type(3 + 5)
<class 'int'>
```

```python
>>> type(3 + 0.5)
<class 'float'>
```
Le type des chaines de caractères est 'str' pour string.

```python
>>> type("J'aime les cookies")
<class 'str'>
```

Lorsque l'on demande le type d'une variable, on cherche alors à savoir le type de la valeur contenue par la variable.

```python
>>> type(3)
<class 'int'>
>>>x = 3
>>> type(x)
<class 'int'>
```

Pareil pour une variable contenant une string.
```python
>>> type('coucou')
<class 'str'>
>>>string = 'coucou'
>>> type(string)
<class 'str'>
```



## La manipulation des chaînes de caractères

Pour rappel une chaîne de caractères est: 
Tout comme les nombres on peut manipuler les chaînes de caractères. Les manipulations les plus simples se font avec les opérateurs + et *.

### La concaténation

La concaténation est la juxtaposition de deux chaînes de caractères pour en créer une nouvelle.

```python
>>> 'Bonjour ' + 'Quentin'
'Bonjour Quentin'
```
Attention:
```python
>>>'Bonjour' + 'Quentin'
'BonjourQuentin'
```

Le ' ' est un caractère!

Vous pouvez concaténer une string contenue dans une variable avec une string.

```python
>>>debut = 'Bonjour '
>>>debut + 'Quentin'
'Bonjour Quentin'
```

Vous pouvez concaténer une string contenue dans une variable avec une string et stocker le résultat dans une nouvelle variable.

```python
>>>debut = 'Bonjour '
>>> phrase = debut + 'Quentin'
>>> phrase
'Bonjour Quentin'
```

### La multiplication

La multiplication est la multiplication d'une chaine de caractères.

```python
>>>x = 'Bonjour'
>>> x * 4
'BonjourBonjourBonjourBonjour'
```

Là encore on peut stocker le résultat dans une variable:

```python
>>>x = 'Bonjour'
>>> beaucoupDeBonjour = x * 4
>>> beaucoupDeBonjour
'BonjourBonjourBonjourBonjour'
```
Ici, on a stocké dans notre variable beaucoupDeBonjour la string 'BonjourBonjourBonjourBonjour'.

Attention avec python, on peut seulement concaténer les strings avec d'autres strings.

```python
>>> x = 1
>>> "J'aime manger" + x + 'cookie'
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can only concatenate str (not "int") to str
```

Ici, j'ai essayé d'avoir le résultat "J'aime manger 1 cookie" en contaténant deux strings et un variable de type int. Cela n'a évidemment pas marché car c'est impossible de concaténer des types str avec d'autres types de variables. 

Par ailleurs, j'ai écrit la chaîne de caractères "J'aime manger". Nous avons vu précédemment que les strings peuvent être déclarées entre simple ou double apostrophes. Avec la string "J'aime manger" nous sommes obligés d'écrire entre double apostrophes. Autrement python nous renverra un message d'erreur syntaxe. l'interpréteur lira ```'J'aime manger'``` de la manière suivante: D'abord il lit le 'J' comme une string à part entière puis il lit ```aime les cookies'``` qui apparaît comme une erreur car non déclaré entre apostrophes.

## Deux classes d'erreurs: Syntaxe et Sémantique

### La syntaxe
La syntaxe est la manière d'écrire du code: 
Exemple: La syntaxe pour écrire une string est de mettre le contenu de la chaine de caractères entre apostrophes.

On a déjà vu une erreur de syntaxe avec ce message d'erreur:


```python
>>>3 = x
  File "<stdin>", line 1
SyntaxError: cannot assign to literal
```

Ici le sens ou la syntaxe de ```3 = x``` exprime que l'on essaie d’assigner la valeur de la variable x à la valeur 3, ce qui n’est pas possible !

### La sémantique

La sémantique est la signification, le sens ou encore le contenu du code. L'ordre de l'assignation des variables fait partie de la sémantique du code.
La sémantique de "J'aime les cookies" est son contenu chaque caractère de la string.

{% include footer_python.html %}

Merci d'avoir lu ce cours. À très vite pour les conditions!

## Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/6){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
