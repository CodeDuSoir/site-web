---
title: "Python - Méthode et Module Exercices"
categories:
  - exercices
  - python
image: "/images/formations/Fondamentaux_Python-method_attributes_moduls-exercices.png"
image_alt: methode-module_exercices_python3
author_staff_member: quentin-parrot
show_comments: true
---

<!-- more -->
Ce cours s'inscrit dans le cadre de notre [formation gratuite à distance à Python](https://www.codedusoir.org/formations/twitch/).

Nous voulons te faire découvrir la programmation en s’amusant et en renforcant tes connaissances en mathématiques.
Retrouve nous les mardis et vendredis à 16h sur [twitch](https://stream.codedusoir.org){:target="_blank"} en direct.


## Exercices

#### Méthode et Module


1. Ecrire un script qui réalise ce calcul: result = racine carrée de [(2 * C * D)/H]. La valeur par défaut de C est 50 et de H est 30. D est un int entré par l’utilisateur via la fonction input. On utilisera le paquet Math et sa methode sqrt()
2. Ecrire une fonction alea3(x, y, z) qui vous renvoie aléatoire un des trois paramètres passées. On utilisera le paquet random et on utilisera sa methode choice().
3. Ecrire un script qui réalise ce calcul: result = racine carrée de [(2 * C * D)/H]. La valeur par défaut de C est 50 et de H est 30. D est un int entré par l’utilisateur via la fonction input. On utilisera le paquet Math et sa methode sqrt()
4. Ecrire une fonction alea3(x, y, z) qui vous renvoie aléatoire un des trois paramètres passées. On utilisera le paquet random et on utilisera sa methode choice().

### Questionnaire 

Tu peux t'autoévaluer [ici](https://forms.interhop.org/node/14){:target="_blank"}. 

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}

