---
title: Apprentissage du code
categories:
  - Code du soir
  - Définition
  - Actualité
image: "/images/apprentissage-code.jpg"
image_alt: apprentissage-code
author_staff_member: axepromotion
show_comments: true
permalink: /apprentissage_code
---

<!-- more -->

# L'apprentissage du code


[Benjamin Mako Hil](https://en.wikipedia.org/wiki/Benjamin_Mako_Hill){:target="_blank"}  et [Dr Sayamindu Dasgupta](https://unmad.in/){:target="_blank"} ont publié un article qui apporte des preuves de l'idée que les enfants peuvent apprendre à coder plus rapidement lorsqu'ils programment dans leur propre langue.

Des millions de jeunes du monde entier apprennent à coder. Souvent, au cours de leur apprentissage, ces jeunes utilisent des langages de programmation basés sur des blocs visuels comme [Scratch](https://scratch.mit.edu/){:target="_blank"}, par exemple. Dans les langages de programmation par blocs, les codeurs manipulent des blocs visuels qui représentent des constructions de code au lieu de symboles textuels et de commandes que l'on trouve dans les langages de programmation plus traditionnels.

Les symboles textuels utilisés dans presque tous les langages de programmation non basés sur des blocs sont tirés de l'anglais - considérez les énoncés "if" et les boucles "for" pour des exemples courants. Les mots clés dans les langages basés sur des blocs peuvent souvent être traduits dans différentes langues. Par exemple, en fonction de la préférence linguistique de l'utilisateur, un ensemble identique d'instructions de calcul en Scratch peut être représenté dans de nombreuses langues différentes.

Bien que les recherches soient axées sur l'apprentissage, **Benjamin Mako Hill** et le **Dr Sayamindu Dasgupta** ont travaillé sur les technologies des langues locales avant de revenir à l'université. Par conséquent, ils ont été tous deux intéressés par la manière dont la traduction croissante des langages de programmation pourrait faciliter l'apprentissage du code par les enfants non anglophones.

Après tout, [un grand nombre de recherches](http://theirworld.org/news/why-teaching-in-mother-tongue-could-help-500m-children){:target="_blank"} sur l'éducation ont montré que l'éducation précoce est plus efficace lorsque l'enseignement est dispensé dans la langue que l'apprenant parle à la maison. Sur la base de ces recherches, les experts ont émis l'hypothèse que les enfants apprenant à coder avec des langages de programmation par blocs traduits dans leur langue maternelle auront de meilleurs résultats d'apprentissage que les enfants utilisant les blocs en anglais.

Ils ont cherché à tester cette hypothèse avec Scratch, une communauté d'apprentissage informelle construite autour d'un langage de programmation basé sur des blocs. Ils ont été aidés par le fait que Scratch est traduit dans de nombreuses langues et compte [un grand nombre d'apprenants dans le monde entier](https://scratch.mit.edu/statistics/#countries){:target="_blank"}. Pour mesurer l'apprentissage, ils se sont appuyés sur [certains de leurs propres travaux antérieurs](https://medium.com/mit-media-lab/studying-the-relationship-between-remixing-learning-c1df54c302df){:target="_blank"}  et ont examiné les répertoires de blocs cumulatifs des apprenants - un peu comme un vocabulaire codé. En observant le répertoire de blocs cumulatifs d'un apprenant au fil du temps, ils ont pu mesurer la vitesse à laquelle son vocabulaire codé s'accroît. En utilisant ces données, ils ont comparé le taux de croissance du répertoire de blocs cumulatifs entre les apprenants de pays non anglophones utilisant le Scratch en anglais et les apprenants des mêmes pays utilisant le Scratch dans leur langue locale. Pour identifier les non-anglophones, ils ont pris en compte les utilisateurs de Scratch qui ont déclaré provenir de cinq pays principalement non anglophones : Portugal, Italie, Brésil, Allemagne et Norvège. Ils ont choisi ces cinq pays parce qu'ils ont chacun une langue très répandue qui n'est pas l'anglais et parce que Scratch est presque entièrement traduit dans cette langue. Même après avoir contrôlé un certain nombre de facteurs tels que l'engagement social sur le site web de Scratch, la productivité des utilisateurs et le temps passé sur les projets, ils ont  constaté que les apprenants de ces pays qui utilisent Scratch dans leur langue locale ont un taux de croissance cumulé du répertoire de blocs plus élevé que leurs homologues qui utilisent Scratch en anglais. Cette croissance plus rapide s'est produite malgré un répertoire initial en bloc plus faible. 

Les résultats des chercheurs sont conformes à ce que les théories de l'éducation disent sur l'apprentissage dans sa propre langue. Leurs résultats représentent également une bonne nouvelle pour les concepteurs de langages de programmation par blocs qui ont déployé des efforts considérables pour rendre leurs langages de programmation traduisibles. C'est aussi une bonne nouvelle pour les bénévoles qui ont passé de nombreuses heures à traduire des blocs et des interfaces utilisateurs. Bien que l'hypothèse des chercheurs soit confirmée, ils soulignent que leurs conclusions sont à la fois limitées et incomplètes. Par exemple, comme ils se concentrent sur l'estimation des différences entre les apprenants de Scratch, leurs comparaisons portent sur des enfants qui ont tous réussi à utiliser Scratch avec succès. Avant la traduction de Scratch, les enfants ayant peu de connaissances pratiques de l'anglais ou de l'écriture latine n'étaient peut-être pas du tout capables d'utiliser Scratch. Grâce à la traduction, beaucoup de ces enfants sont maintenant en capacité d'apprendre à coder.


**Benjamin Mako Hill**, informaticien américain, développeur et chercheur à [la MIT Sloan School of Management](https://fr.wikipedia.org/wiki/Sloan_School_of_Management){:target="_blank"} est aussi militant activiste pour la cause du [logiciel libre](https://fr.wikipedia.org/wiki/Logiciel_libre){:target="_blank"} . Par ailleurs, il est l'auteur de plusieurs ouvrages informatiques. 

**Le Dr Sayamindu Dasgupta** développe de nouveaux outils et expériences qui aident les jeunes apprenants à créer, penser et apprendre avec des données afin qu'ils puissent être des participants actifs et autonomes dans la société de données et de médias dans laquelle ils vivent. Dans ce travail, il cherche à engager les jeunes apprenants non seulement à comprendre et à consommer, mais aussi à créer avec des données et à les remettre en question. 



