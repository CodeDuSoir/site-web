---
title: Les logiciels "libres" et "open sources"
categories:
  - Définition
  - Actualité
  - Libre
image: "/images/formations/libre-opensource.png"
image_alt: libre et opensource
author_staff_member: adrien-parrot
show_comments: true
permalink: /libre-opensource
---

<!-- more -->

Les concepts  « libre » et « open source » sont souvent utilisés comme des synonymes, même par les spécialistes du domaine. Pourtant leurs sens sont différents.

## Open source

"Open" signifie ouvert en anglais. Un logiciel open source est "ouvert" si tout le monde peut accéder à son code source et le redistribuer.

L'open source et l'[Open Source Initiative](https://fr.wikipedia.org/wiki/Open_Source_Initiative){:target="_blank"} (organisation par Bruce Perens et Eric Raymond) se focalisent sur des considérations techniques. 

## Logiciel libre ou FLOSS (Free and Libre Open Source Software)

Le logiciel libre dépasse les considérations techniques et est un mouvement social et politique.

Décrite pour la première fois dans les années 1980 par [Richard Stallman](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html){:target="_blank"}, et popularisée par ses campagnes très fortes (aux opinions très tranchées) via la [Free Software Foundation](https://www.fsf.org/){:target="_blank"} la définition formelle du logiciel libre tient en [quatre libertés](https://vive-gnulinux.fr.cr/logiciel-libre/4-libertes/) :
- La liberté d'exécuter le programme comme vous le souhaitez, et à toute fin (liberté 0)
- La liberté d'étudier le fonctionnement du programme, et le modifier comme vous le souhaitez pour votre ordinateur (liberté 1)
- La liberté de redistribuer les copies que vous avez reçues (liberté 2)
- La liberté de distribuer des copies de vos versions modifiées pour donner à toute la communauté une chance de profiter de vos modifications (liberté 3)

Les logiciels libres sont définis par des licences dites « libres » dont la plus célèbre est la [General Public Licence](https://www.gnu.org/home.fr.html){:target="_blank"}. Celles-ci s'opposent aux licences propriétaires de Microsoft ou d’Apple. 

## En conclusion

Ainsi, le logiciel est forcément open source s'il est libre, mais pas l'inverse !

Pour [Richard Stallman](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html){:target="_blank"}:
> "Les termes « logiciel libre » et « open source » recouvrent à peu près la même gamme de logiciels. Cependant le mouvement du logiciel libre fait campagne pour la liberté des utilisateurs de l'informatique ; c'est un mouvement qui lutte pour la liberté et la justice. L'idéologie open source, par contre, met surtout l'accent sur les avantages pratiques et ne fait pas campagne pour des principes." 

> "L'open source est une méthodologie de développement; le logiciel libre est un mouvement social."

## Sources
- [En quoi l'open source perd de vue l'éthique du logiciel libre](https://www.gnu.org/philosophy/open-source-misses-the-point.fr.html){:target="_blank"}
- [Logiciel libre et open source : les deux concepts sont parfois utilisés de manière interchangeable](https://www.developpez.com/actu/87401/Logiciel-libre-et-open-source-les-deux-concepts-sont-parfois-utilises-de-maniere-interchangeable-mais-quelle-est-la-difference/){:target="_blank"}
- [4 libertés](https://vive-gnulinux.fr.cr/logiciel-libre/4-libertes/){:target="_blank"}


