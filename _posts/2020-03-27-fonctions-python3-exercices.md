---
title: Python - Fonctions
categories:
  - exercices
  - python
  - fondamentaux
  - lycée
image: "/images/formations/fonctions-python3-exercices.png"
image_alt: Fonctions_exercices_python3
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/fonctions-exercices
show_comments: true
---

<!-- more -->

{% include header_python.html %}

## Exercices

#### Fonctions

- Créez une fonction nommée helloWorld(), qui lorsqu'on l'appelle renvoie la string "'Hello World' a été affiché" et affiche "Hello World".
- Créez une fonction nommée cube() qui prend en parametre un nombre et renvoie le cube du nombre.
- Créez une fonction nommée quiEstLePremier() qui résoudra le problème suivant: Un train quitte Paris à 6 h. Il roule à 56 km/h. Un autre train quitte Lyon à 8 h. Il roule à 69 km/h. La distance Paris - Lyon est de 512 km. A quelle heure et à quelle distance de Paris vont-ils se rencontrer ? La fonction aura 4 paramètres: La vitesse du premier train, la vitesse du second train, l'heure de départ du premier train et l'heure de départ du second train. Les valeurs par défaut seront respectivement: 56, 69, 6 et 8.

### Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/8){:target="_blank"}. 

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
