---
title: Python - Listes et boucles while
categories:
  - exercices
  - python
  - fondamentaux
  - lycée
image: "/images/formations/table-while-python3-exercices.png"
image_alt: Liste_While_exercices_python3
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/listes-while-exercices
show_comments: true
---

<!-- more -->
{% include header_python.html %}

## Exercices

#### Listes et While

- Créez une fonction qui prend en argument une liste. Cette fonction affiche toutes les valeurs de la liste et renvoie la dernière valeur de la liste. 
- Créez une liste qui stocke les valeurs des températures journalières de votre ville sur une semaine. Créez une fonction qui parcourt cette liste et renvoie la valeur la plus élevée.
- Créez un script. Via une boucle while, affichez huit fois la chaîne de caractères 'Je dois être affiché 8 fois.'. Après la boucle le script affichera la script 'Merci'.
- Créez une fonction qui prend deux arguments: isTrue et nombrePositif(un nombre positif). Si isTrue est vrai, la fonction affiche tous les nombres compris entre 0 et nombrePositif. Sinon la fonction affiche 'Ca ne marche pas!'. 


### Questionnaire 

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/9){:target="_blank"}. 

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}

