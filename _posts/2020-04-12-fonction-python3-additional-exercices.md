---
title: "Python - Fonctions : Exercice supplémentaire"
categories:
  - exercices
  - python
  - fondamentaux
  - lycée
image: "/images/formations/Fondamentaux_Python-fonctions_additional_Exercices.png"
image_alt: exercices_supplémentaires_fonction_python3
author_staff_member: quentin-parrot
permalink: /fondamentaux-python/fonctions-exercice-supplementaire
show_comments: true
---

<!-- more -->

{% include header_python.html %}

## Exercices

![]({{ site.baseurl }}/images/formations/Fondamentaux_Python-securite-routiere.png)

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [Source de l'exercice](https://www.ac-strasbourg.fr/fileadmin/pedagogie/mathematiques/TICE/Python/Fichier_activites_GFA.pdf){:target="_blank"}
