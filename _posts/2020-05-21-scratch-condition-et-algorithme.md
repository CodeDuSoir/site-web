---
title: "Scratch - Condition et algorithme"
categories:
  - cours
  - scratch
  - fondamentaux
  - école et collège
image: "/images/formations/scratch-cours6.jpg"
author_staff_member: axepromotion
permalink: /fondamentaux-scratch/condition-et-algorithme
---

<!-- more -->

Ce cours s’inscrit dans le cadre de [notre formation gratuite à distance à scratch](https://codedusoir.org/formations/twitch-scratch6/).
Nous voulons te faire découvrir la programmation en s’amusant et en renforçant tes connaissances en mathématiques, en français, en anglais... 

Retrouve-nous les mercredis à 16h [à cette adresse](https://www.twitch.tv/codedusoir){:target="_blank"}.

## L'algorithme

**Le terme algorithme** fut inventé dans le courant du neuvième siècle avant Jésus Christ par le mathématicien Mohammed **Ibn Musa-Al Khwarizmi** (vers 780 à Khiva, Ouzbékistan - vers 850 à Bagdad).

Mohammed ibn Musa al-Khwarizmi est le premier des mathématiciens persans, et sans doute le plus connu. Il vit à Bagdad du temps de la splendeur de la dynastie abbasside. 
Le calife al-Mamum qui règne sur l'empire encourage les sciences et les arts. Il crée le premier observatoire permanent au monde, il fonde une maison de la sagesse où al-Khwarizmi et d'autres traduisent des textes scientifiques et philosophiques grecs, et étudient, à partir de ceux-ci, astronomie, algèbre et géométrie.
Al-Khwarizmi est aussi un des pionniers de l'algèbre.
Le premier mérite d'al-Khwarizmi est d'avoir été un formidable passeur de connaissance.

![](/images/formations/scratch-cours6-ibn-Musa-al-Khwarizmi.jpg)


Dans le domaine des mathématiques, dont **le terme algorithme** est originaire, un algorithme peut être considéré comme un ensemble d’opérations ordonné et fini devant être suivi dans l’ordre pour résoudre un problème. 
En guise d’exemple très simple, prenons une recette de cuisine. Dans chaque recette, une procédure spécifique doit être suivie dans l’ordre. 
Les différentes étapes de la recette représentent les opérations qui constituent l’algorithme.

Avec l’essor de l’intelligence artificielle [à cette adresse](https://www.lebigdata.fr/intelligence-artificielle-et-big-data){:target="_blank"}, ce terme est de plus en plus utilisé et a rejoint la catégorie des *buzzwords*


Aujourd’hui, on utilise des algorithmes pour trouver l’amour, pour investir dans les meilleures actions, pour prédire le crime, pour organiser les résultats de recherche sur le web et pour des millions d’autres applications. 
**Les algorithmes sont omniprésents et dirigent notre économie, notre société et peut-être même la façon dont nous pensons**.

## La condition

Dans un deuxième temps, nous allons comprendre comment utiliser la condition dans la programmation.
Vous entendrez parler d'**if** en anglais que l'on traduit par ***si** en français. 
Vous avez bien compris que, dans une phrase, **si** introduit la condition.

## Pratique

- D'abord, rendons-nous sur notre compte scratch [à cette adresse](https://scratch.mit.edu/){:target="_blank"}.
- Allons également sur notre forum pour poser tes questions ou avoir accès à des liens, ou à d'autres ressources [à cette adresse](https://codedusoir.zulipchat.com){:target="_blank"}. C'est là que tu pourras discuter avec nous et demander de l'aide si tu as besoin.
- Avant de commencer nous nous rendons sur mon "compte enseignant" pour visualiser les ressources mises à disposition.

**Dans le 6ème cours, en pratique, nous allons faire réagir un lutin quand il touche un autre lutin.**
Nous allons écrire un programme pour que le lutin effectue une action quand quelque chose se produit.
Allons chercher le lutin *convertible*, déposons-le sur la scène et réduisons-le !
Puis allons chercher le lutin *city bus* et réduisons-le dans les mêmes proportions que le lutin *convertible*.
Ensuite, plaçons les lutins de manière à ce que la voiture touche le bus.
On va faire parler le chauffeur de la voiture quand celle-ci touche le bus.
Maintenant, on va écrire notre programme
Nous allons dans *évènement* pour choisir *quand drapeau vert cliqué*
Puis dans *contrôle* pour choisir **la condition** grâce à *si alors* que nous déposons en dessous de  *quand drapeau vert cliqué*
Dans *capteur*, nous trouvons *touche le pointeur de la souris*, mais si on déroule le menu, nous trouvons *touche City bus*. Voilà notre condition !
Mais vous voyez la zone vide dans *si alors*, 
Pour faire parler conducteur, vous vous souvenez... Il nous faut nous rendre dans *apparence*, choisir *dire bonjour pendant 2 secondes* et écrire ce que dit le chauffeur (Zut ! J’ai réussi. C’est cassé maintenant)
Nous venons de créer une condition. Pour que ça marche, les blocs introduit dans le bloc ‘’si alors’’ ne sont activé que si le contenu du bloc capteur est vérifié.
On va à Contrôle pour prendre le bloc *répéter indéfiniment* et le déposer autour des autres blocs en dehors de *quand drapeau vert pressé*. Comme ça le programme vérifiera que la conditions est remplie
Déplaçons la voiture pour qu’elle ne touche plus le *city bus* et vérifions : il ne se passe rien car la condition n’est plus remplie.

**Ajoutons une nouvelle action**
On va chercher le lutin *convertible2*  que l'on réduit dans les mêmes proportions que les autres lutins.
Puis on ajoute un son quand le conducteur de *convertible rencontre *convertible 2*
Après on refait la même procédure sauf que le bloc capteur indique *touche le pointeur de la souris, touche convertible2*
Contrôle/si alors en dessous et mettre en évidence la zone vide de si alors
Pour mettre en oeuvre une autre condition on va chercher un son dans **l'onglet son** à droite de l'onglet code son. On choisit le son *C Bass*.
Nous revevenons à l'onglet **code ou script** et dans les blocs de **son** nous choisissons *jouer le son C Bass jusqu’au bout*
Puis dans contrôle on choisit *répéter indéfiniment* que l'on dépose autour des autres blocs en dehors de quand drapeau vert pressé. Comme ça le programme vérifiera que la condition est bien remplie


Amusez-vous !

Peux-tu établir un nouveau programme en créant un arrière plan tout d'abord. Ensuite raconte l'histoire d'un papillon qui renconcre un ours... En t'inspirant de l'image ci-dessous.

![](/images/formations/scratch-cours6-pratique.jpg)

Tu peux aussi utiliser ta voix pour faire parler ton lutin. Tu peux lui faire faire demi-tour quand Julie arrive au bout de la scène... Bref ! utilise tout ce que tu sais le faire ;-)

N'oublie pas que tu peux demander de l'aide sur le forum

Retrouve-nous les mercredis à 16h sur [twitch](https://www.twitch.tv/codedusoir){:target="_blank"} en direct.

Tu m'enverras un message sur notre forum pour partager tes impressions et tes réalisations. Je suis très intéressée 
N'oublie pas, si tu as besoin d'un coup de main, nous sommes là pour t'aider.

Merci d'avoir lu ce cours.

## Questionnaire 
Evalue tes connaissances et visualise ta progression [ici](https://form.codedusoir.org/node/17){:target="_blank"}.

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles

- [forum](https://codedusoir.zulipchat.com){:target="_blank"}
- [twitch](https://www.twitch.tv/codedusoir/){:target="_blank"}
- [edx](https://courses.edx.org/login?next=/courses/course-v1%3ADelftX%2BScratchTENGX%2B1T2019/courseware/e0ad11486de74ade95bbc327bfe84d7c/2717a2fdaeb94fa1bae1b4674ab55cde/%3Factivate_block_id%3Dblock-v1%253ADelftX%252BScratchTENGX%252B1T2019%252Btype%2540sequential%252Bblock%25402717a2fdaeb94fa1bae1b4674ab55cde){:target="_blank"}