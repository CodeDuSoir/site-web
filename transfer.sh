#!/bin/bash

cd /home/quentin/Documents/Dev/CodeDuSoir/site-web
TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`
DATA=`ls futurePosts`
DATA=`echo "$DATA" | sed ':a;N;$!ba;s/\n/, /g'`


	git remote set-url origin git@framagit.org:CodeDuSoir/site-web.git
	git pull
	if [[ $1 = "" ]]; then
	#	echo "Je post tous les fichiers dans le dossier futurePosts"
		mv `ls -d futurePosts/*.md | sed "s/futurePosts\/README.md//"`  _posts/
		mv ./futurePosts/*.png images/formations/
		git add _posts/* images/formations/* futurePosts/*
		git commit -m "add $DATA to the directory _posts for .md files and to the images/formations directory for .png files. made at $TIMESTAMP"
		git push -u origin master
	else
		for ARG in $@; do
			if [[ ! "$ARG" = *".md" && ! "$ARG" = *".png" ]]; then
				echo -e "Le format du fichier $ARG n'est pas accepté! \nLa fonctions transfer n'accepte que les fichiers markdown et png. \nFaites en sorte que votre fichier $ARG ait une extension png ou md!"  
				exit 1
			fi
		done
		for ARG in $@; do
			if [[ "$ARG" = *".md" ]]; then
	#			echo "Je deplace le fichier $ARG du dossier futurePosts dans md"
				mv futurePosts/$ARG _posts/
			elif [[ "$ARG" = *".png" ]]; then
	#			echo "Je deplace le fichier $ARG du dossier futurePosts au dossier image"
				mv futurePosts/$ARG images/formations/
			fi
		done
		git add _posts/* images/formations/* futurePosts/*
		git commit -m "add $DATA to the directory _posts for .md files and to the images/formations directory for .png files. made at $TIMESTAMP"
		git push -u origin master
	fi
