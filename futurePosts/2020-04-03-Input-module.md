---
title: Python - Inputs et Modules
categories:
  - cours
  - python
  - fondamentaux
  - lycée
  - exercices
image: "/images/formations/inputs-modules-python3-exercices.png"
image_alt: Input_Modules_cours_python3_Exercices
video_url: ""
author_staff_member: quentin-parrot
show_comments: true
---

<!-- more -->

{% include header_python.html %}
{% include video.html video_url=page.video_url %}

## Exercices

#### inputs et modules

1. créez une fonction fibo() sans paramètre. Qui demande à l'utilisateur "Quelle est l'index du nombre que vous cherchez dans la suite de fibonacci?". La fonction vous renvoie et affiche le nombre demandé. Exemple:

```python
>>>Quelle est l'index du nombre que vous cherchez dans la suite de fibonacci? 7
>>>13
```

2. Créez une fonction isPrime() qui prend en parametre un int. Cettre fonction vous renvoie True si le nombre est premier. et False si le nombre n'est pas premier.
3. Créez une fonction hundredPrime() qui affiche les cent premiers nombres premiers. 


## Questionnaire  

Tu peux t'autoévaluer [ici](https://form.codedusoir.org/node/10){:target="_blank"}. -->

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/).

## Liens utiles
- [forum](http://forum.codedusoir.org){:target="_blank"}
- [twitch](http://stream.codedusoir.org){:target="_blank"}
- [OpenClassRoom - Python](https://openclassrooms.com/fr/courses/235344-apprenez-a-programmer-en-python/230722-faites-vos-premiers-pas-avec-linterpreteur-de-commandes-python){:target="_blank"}
- [repl.it](https://repl.it){:target="_blank"}
