#!/bin/bash


TIMESTAMP=`date "+%Y-%m-%d %H:%M:%S"`
DATA=`ls`
DATA=`echo "$DATA" | sed ':a;N;$!ba;s/\n/, /g'`


	git remote set-url origin https://framagit.org/CodeDuSoir/site-web.git
	git pull
	if [[ $1 = "" ]]; then
	#	echo "Je post tous les fichiers dans le dossier futurePosts"
		mv `ls -d *.md | sed "s/\/README.md//"`  ../_posts/
		mv *.png ../images/formations/
		git rm *.md *.png
		git add ../_posts/* ../images/formations/* *
		git commit -m "add $DATA to the directory _posts for .md files and to the images/formations directory for .png files. made at $TIMESTAMP"
		git push -u origin master
	else
		for ARG in $@; do
			if [[ ! "$ARG" = *".md" && ! "$ARG" = *".png" ]]; then
				echo -e "Le format du fichier $ARG n'est pas accepté! \nLa fonctions transfer n'accepte que les fichiers markdown et png. \nFaites en sorte que votre fichier $ARG ait une extension png ou md!"  
				exit 1
			fi
		done
		for ARG in $@; do
			if [[ "$ARG" = *".md" ]]; then
	#			echo "Je deplace le fichier $ARG du dossier futurePosts dans md"
				mv $ARG ../_posts/
				git rm $ARG 
			elif [[ "$ARG" = *".png" ]]; then
	#			echo "Je deplace le fichier $ARG du dossier futurePosts au dossier image"
				mv $ARG ../images/formations/
				git rm $ARG
			fi
		done
		git add ../_posts/* ../images/formations/* *
		git commit -m "add $DATA to the directory _posts for .md files and to the images/formations directory for .png files. made at $TIMESTAMP"
		git push -u origin master
	fi
