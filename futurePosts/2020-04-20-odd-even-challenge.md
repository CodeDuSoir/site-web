---
title: "Pair (even) ou impair (odd)"
categories:
  - challenges
  - python
  - fondamentaux
  - lycée
image: "/images/formations/python-challenge.png"
author_staff_member: adrien-parrot
permalink: /challenges/odd_even
---

<!-- more -->

{% include header_challenge_python.html %}

# Consignes

- Function name : is_even
- Allowed functions : [input()](https://docs.python.org/fr/3/library/functions.html#input), [int()](https://docs.python.org/fr/3/library/functions.html#int), [float()](https://docs.python.org/fr/3/library/functions.html#float), [is_integer()](https://python-reference.readthedocs.io/en/latest/docs/float/is_integer.html)
- Prerequis : [Variables]({{ site.baseurl }}/fondamentaux-python/operateurs-variables), [Conditions]({{ site.baseurl }}/fondamentaux-python/script-conditions), [Functions]({{ site.baseurl }}/fondamentaux-python/operateurs-variables), [Built-In Functions]({{ site.baseurl }}/fondamentaux-python/fonctions-predefinies)

- Créer un programme python qui détermine si un nombre est pair ou impair en fonction de l’entrée de l’utilisateur.
Tu dois écrire le code de la fonction is_even(number) !

# Aides

```python
>>> 2 % 2
0
>>> 3 % 2
1
>>> 4 % 2
0
```

# Rendu

### Par repl.it (en s'identifiant)

<iframe frameborder="0" width="100%" height="600px" src="https://repl.it/student_embed/assignment/5115170/736274e45627f1bdad97ce8c56b1f7cd"></iframe>

Si l'affichage n'est pas correct, voici le [lien d'invitation](https://repl.it/classroom/invite/owOeEyb){:target="_blank"}.

### Par mail (sans s'identifier ) à codedusoir@protonmail.com

Le fichier attendu en pièce jointe doit se nommer main.py et il doit contenir le code suivant
```python
def is_even(number):
  # return a boolean
  ...
  ...
  return ...

if __name__ == '__main__':
    try:
        num = float(input("Entrer un integer : "))
        if num.is_integer():
            if is_even(int(num)):
              print("Ce nombre est pair.")
            else:
              print("Ce nombre est impair.")
        else:
            print("Merci d'entrer un integer")            
    except ValueError:
        print("Merci d'entrer un integer")              
```

```python
$> python3 main.py
Entrer un integer : 2
Ce nombre est pair. 
```

# On garde le contact

{% include footer_python.html %}

Si tu veux un coup de main, laisse nous [ton mail]({{ site.baseurl }}/contact/) ou contacte-nous sur [forum](http://forum.codedusoir.org){:target="_blank"}.
