---
name: Scratch3
order: 4
subtitle: Apprends Scratch3 lors de nos directs !
external_url: https://scratch.mit.edu/
image_path: /images/formations/twitch-scratch3.png
---

## Quand, quoi ?

Ce cours a pour objectif principal de vous faire apprendre les bases de la programmation (boucles, variables, structures de données) avec Scratch3. Il s'adresse aux parents, aux enseignants et aux enfants.

Ce cours est fait pour toi ! Nous voulons te faire découvrir la programmation en s’amusant et en créant pour pouvoir appréhender le monde de demain.

Retrouve nous en direct les mercredis à 16h.

<a style="text-align: center;" class="button alt" type="blue" target="_blank" href="https://www.twitch.tv/codedusoir">Accès au live</a>

## Comment ?

Avec [Scratch3](https://scratch.mit.edu/){:target="_blank"}, [Twitch](https://twitch.tv/codedusoir){:target="_blank"} tout est en ligne. Tu n'as rien à installer !

## Scratch
- La programmation
- Premier programme en Scratch
- Les signaux
- Les listes
- Le hasard
- Les variables
- Les boucles
- Les robots
