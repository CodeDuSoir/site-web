---
name: ScratchJunior
order: 1
subtitle: Mettre en classe ScratchJunior !
external_url: https://www.scratchjr.org/
image_path: /images/formations/screenshot-scratch-junior.jpg
---

## Titre
De la prise en main à la mise en classe de ScratchJunior

## Contexte
Depuis [2015](http://cache.media.education.gouv.fr/file/MEN_SPE_11/67/3/2015_programmes_cycles234_4_12_ok_508673.pdf), la logique algorithmique est présente tout au long du parcours de cycles. 
<br>
ScratchJunior pour est une application gratuite entièrement inspirée du concept initial de [Scratch](https://scratch.mit.edu/){:target="_blanck"}.
C’est un langage de programmation visuel et simplifié, approprié aux enfants de moins de 8 ans

## Objectifs
- Rappeler le cadre institutionnel : construire des séances reposant sur une pédagogie projet en conformité avec les objectifs des programmes cycles [2](https://eduscol.education.fr/pid34139/cycle-2.html){:target="_blank"} et [3](https://eduscol.education.fr/pid34150/cycle-3-ecole-elementaire-college.html){:target="_blank"}
- Découvrir les bases de l'algorithmique
- Découvrir ScratchJunior : découvrir l'environnement ScratchJunior et ses principales fonctionnalités
- Dévenir autonome dans sa classe, idées d'activités de programmation en classe

## Contenu
- Auto-évaluations initiale et finale
- Explication des programmes des cycles [2](https://eduscol.education.fr/pid34139/cycle-2.html){:target="_blank"} et [3](https://eduscol.education.fr/pid34150/cycle-3-ecole-elementaire-college.html){:target="_blank"}
- Définition d’un algorithme, et principe du codage
- Prise en main et manipulation du logiciel ScratchJunior : environnment, commandes de bases
- Activité debranchée et branchée, création d'une histoire avec ScratchJunior, résolution de petits problèmes

## Méthodes 
- Apprentissage multimodal : combiner formation en présentiel et en ligne
- Travaux de groupes (maximum 12), et individuel

## Pré-requis
- Savoir les bases de l'informatique : créer un compte en ligne, envoyer un e-mail, créer/copier/supprimer des fichiers, savoir utiliser une messagerie instantanée

## Public visé
- Enseignants du primaire.
- Toute personne souhaitant se former à l'algorithmique via ScratchJunior et par le jeu

## Durée 
- 2 heures de formation individuelle à distance
- Deux journées en présentiel
- Un suivi mensuel

## Budget
500 euros
