---
name: Python3
order: 3
subtitle: Apprends Python3 lors de nos directs ! 
external_url: https://www.python.org/
image_path: /images/formations/twitch-python3.png
---

## Quand, quoi ?

Ce cours a pour objectif principal de vous faire apprendre les bases de la programmation en Python. Nous nous intéressons aux bases des mathématiques (niveau fin de collège, début Lycée).

Tu es lycéen en classe de seconde, ce cours est fait pour toi ! Nous voulons te faire découvrir la programmation en s’amusant et en renforcant tes connaissances en mathématiques.

Retrouve nous en direct les mardis et vendredis à 16h.

<a style="text-align: center;" class="button alt" type="blue" target="_blank" href="https://www.twitch.tv/codedusoir">Accès au live</a>

## Comment ?

Avec [Repl.it](https://docs.repl.it/misc/quick-start){:target="_blank"}, [Twitch](https://twitch.tv/codedusoir){:target="_blank"} tout est en ligne. Tu n'as rien à installer !

## Python
- [Prise en main]({{ site.baseurl }}/fondamentaux-python/operateurs-variables)
- [Les variables]({{ site.baseurl }}/fondamentaux-python/operateurs-variables)
- [Les types]({{ site.baseurl }}/fondamentaux-python/type-manipulation-string)
- [Les manipulations de chaîne de caractères]({{ site.baseurl }}/fondamentaux-python/type-manipulation-string)
- [Les conditions]({{ site.baseurl }}/fondamentaux-python/script-conditions)
- [Les fonctions]({{ site.baseurl }}/fondamentaux-python/fonctions)
- [Les fonctions prédéfinies]({{ site.baseurl }}/fondamentaux-python/fonctions-predefinies)
- [Les boucles non bornées]({{ site.baseurl }}/fondamentaux-python/boucles-non-bornees)
- Les boucles bornées
- L'algèbre de Boole
- Import de fonctions / variables
- Les variables avancées
- Les probabilités avec python
- Les statistiques avec python
- ...
