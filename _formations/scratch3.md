---
name: Scratch3
order: 2
subtitle: Mettre en classe Scratch3 !
external_url: https://scratch.mit.edu/
image_path: /images/formations/screenshot-scratch3.png
---

## Titre
De la prise en main à la mise en classe de Scratch3

## Contexte
Depuis [2015](http://cache.media.education.gouv.fr/file/MEN_SPE_11/67/3/2015_programmes_cycles234_4_12_ok_508673.pdf), la logique algorithmique est présente tout au long du parcours de cycles.
Scratch pour est une application web gratuite permettant d'utiliser langage de programmation visuel et simplifié, approprié aux enfants de 8 à 16 ans

## Objectifs
- Rappeler le cadre institutionnel : construire des séances reposant sur une pédagogie projet en conformité avec les objectifs des programmes cycles [3](https://eduscol.education.fr/pid34150/cycle-3-ecole-elementaire-college.html){:target="_blank"} et [4](https://eduscol.education.fr/pid34185/cycle-4-college.html){:target="_blank"}.
- Découvrir les bases de l'algorithmique
- Découvrir Scratch 3 : découvrir l'environnement Scratch et ses principales fonctionnalités
- Se perfectionner à Scratch 3 : utilisation des fonctionnalités avancées (tableaux et fonctions)
- Dévenir autonome dans sa classe, idées d'activités de programmation en classe

## Contenu
- Auto-évaluations initiale et finale
- Explication des programmes des cycles [3](https://eduscol.education.fr/pid34150/cycle-3-ecole-elementaire-college.html){:target="_blank"} et [4](https://eduscol.education.fr/pid34185/cycle-4-college.html){:target="_blank"}
- Structure d’un ordinateur, définition d’un algorithme
- Prise en main et manipulation du logiciel Scratch3 : environnement, commandes de bases, tableaux, fonctions, programmation événementielle
- Activité debranchée et branchée, gamification, pédagogie par projet

## Méthodes 
- Apprentissage multimodal : combiner formation en présentiel et en ligne
- Travaux de groupes (maximum 12), et individuel

## Pré-requis
- Se créer un [compte Scratch](https://www.ac-paris.fr/portail/jcms/p1_1503980/un-compte-scratch-pour-l-enseignant){:target="_blank"}  pour éducateur : [https://scratch.mit.edu/educators/#teacher-accounts](https://scratch.mit.edu/educators/#teacher-accounts){:target="_blank"}
- Savoir les bases de l'informatique : créer un compte en ligne, envoyer un e-mail, créer/copier/supprimer des fichiers, savoir utiliser une messagerie instantanée

## Public visé
- Enseignants du primaire, collège
- Toute personne souhaitant se former à l'algorithmique via Scratch et par le jeu

## Durée 
- 2 heures de formation individuelle à distance
- Deux journées en présentiel
- Un suivi mensuel

## Budget
500 euros
