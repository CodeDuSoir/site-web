---
name: Python3
order: 3
title: lol
subtitle: Mettre en classe Python3 !
external_url: https://www.python.org/
image_path: /images/formations/screenshot-jupyter-python.jpg
---

## Titre
De la prise en main à la mise en classe de Python 3

## Contexte
En 2017-2018, le programme de mathématiques de seconde a été changé, avec une nouvelle rubrique "Algorithmique et programmation". 
Python est un langage de programmation, facile à apprendre, est recommandé pour cet enseignement.

## Objectifs
- Rappeler le cadre institutionnel : construire des séances reposant sur une pédagogie projet en conformité avec les objectifs des programmes de [Mathématiques](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/95/7/spe631_annexe_1062957.pdf){:target="_blank"}, de [Sciences Numérique et Technologique](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf){:target="_blank"} (SNT) et de [Numérique et Sciences Informatiques](http://cache.media.education.gouv.fr/file/CSP/41/2/1e_Numerique_et_sciences_informatiques_Specialite_Voie_G_1025412.pdf){:target="_blank"} (NSI)
- Découvrir les bases de l'algorithmique : variables, boucles, conditions, functions
- Télécharger et utiliser un [environnement de développement](https://fr.wikipedia.org/wiki/Environnement_de_d%C3%A9veloppement){:target="_blank"} pour Python
- Découvrir le langage de programmation Python de manière à pouvoir l'utiliser en classe
- Fournir de nombreuses idées d'activités de programmation à faire en classe

## Contenu
- Explication des programmes de [Mathématiques](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/95/7/spe631_annexe_1062957.pdf){:target="_blank"}, de [SNT](https://cache.media.education.gouv.fr/file/SP1-MEN-22-1-2019/08/5/spe641_annexe_1063085.pdf){:target="_blank"} et de [NSI](http://cache.media.education.gouv.fr/file/CSP/41/2/1e_Numerique_et_sciences_informatiques_Specialite_Voie_G_1025412.pdf){:target="_blank"}
- Auto-évaluations initiale et finale
- Structure d’un ordinateur et des inferfaces hommes-machines, définition d’un algorithme, des languages de programmation et des données
- Programmation en Python de problèmes simples de mathématiques 
- Gamification, pédagogie par projet (interface graphique Tkinter)

## Méthodes 
- Apprentissage multimodal : combiner formation en présentiel et en ligne
- Travaux de groupes (maximum 12), et individuel

## Pré-requis 
- Savoir les bases de l'informatique : créer un compte en ligne, envoyer un e-mail, créer/copier/supprimer des fichiers, savoir utiliser une messagerie instantanée

## Public visé
- Enseignants du lycée
- Toute personne souhaitant se former à l'algorithmique via Python et par le jeu

## Durée 
- 2 heures de formation individuelle à distance
- Deux journées en présentiel
- Un suivi mensuel

## Budget
500 euros
